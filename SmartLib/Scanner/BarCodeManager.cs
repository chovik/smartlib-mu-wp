﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZXing;
using ZXing.OneD;
using ZXing.QrCode;
using ZXing.Datamatrix;

namespace WP7.ScanBarCode
{
    /// <summary>
    /// Scan a barcode for a live video stream
    /// </summary>
    public static class BarCodeManager
    {
        internal static Func<Result, Task<bool>> _onBarCodeFound;
        internal static Action<Exception> _onError;
        
        static BarCodeManager()
        {
            MaxTry = 15;   
        }

        /// <summary>
        /// Starts the scan : navigates to the scan page and starts reading video stream
        /// Note : Scan will auto-stop if navigation occurs
        /// </summary>
        /// <param name="onBarCodeFound">Delegate Action on a barcode found</param>
        /// <param name="onError">Delegate Action on error</param>
        /// <param name="zxingReader">(optional) A specific reader format, Default will be EAN13Reader </param>
        public static void StartScan(Func<Result, Task<bool>> onBarCodeFound, Action<Exception> onError, BarcodeFormat? barcodeFormat = null)
        {
            
            var _mainFrame = Application.Current.RootVisual as PhoneApplicationFrame;
            if (_mainFrame != null)
            {
                if (barcodeFormat == null)
                {
                    barcodeFormat = BarcodeFormat.All_1D;
                }
                _onBarCodeFound = onBarCodeFound;
                _onError = onError;
                _ZXingReader = GetReader(barcodeFormat.Value);

                _mainFrame.Navigate(new Uri("/MVVM/Views/BarCode.xaml", UriKind.Relative));
            }
        }

        /// <summary>
        /// Try 20 times to focus and scan for 1,5 sec (default)
        /// </summary>
        public static int MaxTry
        {
            get;
            set;
        }

        private static Reader _ZXingReader;
        internal static Reader ZXingReader
        {
            get
            {
                if (_ZXingReader == null)
                    return new EAN13Reader();
                return _ZXingReader;
            }

            set
            {
                _ZXingReader = value;
            }
        }

        /// <summary>
        /// Returns the zxing reader class for the current specified ScanMode.
        /// </summary>
        /// <returns></returns>
        internal static Reader GetReader(BarcodeFormat format)
        {
            Dictionary<DecodeHintType, object> zxingHints
                = new Dictionary<DecodeHintType, object>() { { DecodeHintType.TRY_HARDER, true } };
            Reader r;
            switch (format)
            {
                case BarcodeFormat.CODE_128:
                    r = new Code128Reader();
                    break;
                case BarcodeFormat.CODE_39:
                    r = new Code39Reader();
                    break;
                case BarcodeFormat.EAN_13:
                    r = new EAN13Reader();
                    break;
                case BarcodeFormat.EAN_8:
                    r = new EAN8Reader();
                    break;
                case BarcodeFormat.ITF:
                    r = new ITFReader();
                    break;
                case BarcodeFormat.UPC_A:
                    r = new UPCAReader();
                    break;
                case BarcodeFormat.UPC_E:
                    r = new UPCEReader();
                    break;
                case BarcodeFormat.QR_CODE:
                    r = new QRCodeReader();
                    break;
                case BarcodeFormat.DATA_MATRIX:
                    r = new DataMatrixReader();
                    break;

                case BarcodeFormat.All_1D:
                    r = new MultiFormatOneDReader(zxingHints);
                    break;
                
                //Auto-Detect:
                case BarcodeFormat.UPC_EAN_EXTENSION:
                default:
                    r = new MultiFormatUPCEANReader(zxingHints);
                    break; 
            }
            return r;
        }
    }
}
