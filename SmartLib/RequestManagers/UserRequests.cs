﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RestSharp;
using System.Threading.Tasks;
using SmartLib.RequestManagers;
using System.Diagnostics;
using System.Net.Browser;

namespace SmartLib.Core
{
    public class UserRequests : Requests
    {
        private RestClient client = null;

        public UserRequests(string url)
        {
            client = new RestClient(url);
        }

        public UserRequests(RestClient client)
        {
            this.client = client;
        }

        private RestRequest CreateCheckAuthenticationRequest()
        {
            var request = new RestRequest("user/authentication", Method.POST);
            request.RequestFormat = DataFormat.Json;
            return request;
        }

        private RestRequest CreateLogInRequest(string uco, string password)
        {
            var request = new RestRequest("user/login", Method.POST);
            request.AddParameter("uco", uco);
            request.AddParameter("password", password);
            return request;
        }

        private RestRequest CreateChangePasswordRequest(string uco, string oldPassword, string newPassword)
        {
            var request = new RestRequest("user/changepassword", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddParameter("uco", uco);
            request.AddParameter("oldPassword", oldPassword);
            request.AddParameter("newPassword", newPassword);
            return request;
        }

        private RestRequest CreateSignUpRequest(string uco, string firstName, string lastName)
        {
            var request = new RestRequest("user/registration", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddParameter("uco", uco);
            request.AddParameter("firstName", firstName);
            request.AddParameter("lastName", lastName);
            return request;
        }

        private RestRequest CreateLogOutRequest()
        {
            var request = new RestRequest("user/logout", Method.POST);
            request.RequestFormat = DataFormat.Json;
            return request;
        }

        private RestRequest CreateLostPasswordRequest(string uco)
        {
            var request = new RestRequest("user/lostpassword", Method.POST);
            request.AddParameter("uco", "#" + uco);
            return request;
        }

        public async Task<HttpStatusCode> ChangePassword(string uco, string oldPassword, string newPassword)
        {
            if (uco == null)
                throw new ArgumentNullException("uco");
            if (oldPassword == null)
                throw new ArgumentNullException("oldPassword");
            if (newPassword == null)
                throw new ArgumentNullException("newPassword");

            HttpStatusCode statusCode = HttpStatusCode.NotFound;

            try
            {
                var request = CreateChangePasswordRequest(uco, oldPassword, newPassword);
                var response = await client.GetResponseAsync(request);
                statusCode = response.WebResponseToHTTPStatusCode();

                //process returned status code (from server response)
                switch (statusCode)
                {
                    case HttpStatusCode.OK:
                        OnSuccess("Password has been changed.");
                        break;
                    case HttpStatusCode.Unauthorized:
                        OnError("Need authorization.");
                        App.CurrentApplication.LoggedIn = false;
                        App.CurrentApplication.LoggedUco = null;
                        break;
                    default:
                        Debug.WriteLine("Change password - Unxpected Status Code '" + statusCode + "'.");
                        App.CurrentApplication.LoggedIn = false;
                        App.CurrentApplication.LoggedUco = null;
                        break;
                }
            }
            catch (WebException ex)
            {
                OnError("The server is not responding , please check your connection and try again.");
            }

            return statusCode;
        }

        public async Task<HttpStatusCode> Login(string uco, string password)
        {
            if (uco == null)
                throw new ArgumentNullException("uco");
            if (password == null)
                throw new ArgumentNullException("password");

            HttpStatusCode statusCode = HttpStatusCode.NotFound;

            try
            {
                bool httpResult = HttpWebRequest.RegisterPrefix("http://", WebRequestCreator.ClientHttp);
                httpResult = HttpWebRequest.RegisterPrefix("https://", WebRequestCreator.ClientHttp);
                var request = CreateLogInRequest(uco, password);
                var response = await client.GetResponseAsync(request);
                statusCode = response.WebResponseToHTTPStatusCode();

                switch (statusCode)
                {
                    case HttpStatusCode.OK:
                        OnSuccess("You were successfuly logged in.");
                        break;
                    case HttpStatusCode.Unauthorized:
                        OnError("Wrong password.");
                        break;
                    default:
                        Debug.WriteLine("Unxpected Status Code '" + statusCode + "'.");
                        break;
                }
            }
            catch (WebException ex)
            {
                OnError("The server is not responding , please check your connection and try again.");
            }

            return statusCode;
        }

        public async Task<HttpStatusCode> LogOut()
        {
            var request = CreateLogOutRequest();
            var response = await client.GetResponseAsync(request);
            return response.WebResponseToHTTPStatusCode();
        }

        public async Task<HttpStatusCode> CheckAuthentication()
        {
            var request = CreateCheckAuthenticationRequest();
            var response = await client.GetResponseAsync(request);
            return response.WebResponseToHTTPStatusCode();
        }

        public async Task<HttpStatusCode> SignUp(string uco, string firstName, string lastName)
        {
            if (firstName == null)
                throw new ArgumentNullException("firstName");
            if (lastName == null)
                throw new ArgumentNullException("lastName");

            if (string.IsNullOrWhiteSpace(firstName))
                throw new ArgumentException("firstName");
            if (string.IsNullOrWhiteSpace(lastName))
                throw new ArgumentException("lastName");

            HttpStatusCode statusCode = HttpStatusCode.NotFound;

            try
            {
                var request = CreateSignUpRequest(uco, firstName, lastName);
                var response = await client.GetResponseAsync(request);

                statusCode = response.WebResponseToHTTPStatusCode();

                switch (statusCode)
                {
                    case HttpStatusCode.OK:
                        OnSuccess("Registration was successful.");
                        break;
                    case HttpStatusCode.BadRequest:
                        OnError("Student with filled UCO does not exists");
                        break;
                    case HttpStatusCode.Conflict:
                        OnError("User with the same UCO already exists.");
                        break;
                    default:
                        Debug.WriteLine("Unxpected Status Code '" + statusCode + "'.");
                        break;
                }
            }
            catch (WebException ex)
            {
                OnError("The server is not responding , please check your connection and try again.");
            }

            return statusCode;
        }

        public async Task<HttpStatusCode> LostPassword(string uco)
        {
            if (uco == null)
                throw new ArgumentNullException("uco");

            HttpStatusCode statusCode = HttpStatusCode.NotFound;
            
            var request = CreateLostPasswordRequest(uco);
            var response = await client.GetResponseAsync(request);
            statusCode = response.WebResponseToHTTPStatusCode();                

            return statusCode;
        }
    }
}
