﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RestSharp;
using System.Linq;
using System.Threading.Tasks;
using SmartLib.Helpers;
using SmartLib.RequestManagers;

namespace SmartLib.Core
{
    public class RESTManager
    {
        private RestClient client;

        public RESTManager(string url)
	    {
            ArgumentValidator.AssertNotNullOrWhiteSpaceString(url, "url");
            client = new RestClient(url);
	    }

        private static HttpStatusCode WebResponseToHTTPStatusCode(IRestResponse response)
        {
            if (response == null)
                throw new ArgumentNullException("response");

            var statusCodeParameters = response.Headers.Where(parameter => parameter.Name == "StatusCode");

            if (statusCodeParameters.Count() > 0)
            {
                var statusCodeParameter = statusCodeParameters.First();
                return (HttpStatusCode)Enum.Parse(typeof(HttpStatusCode), statusCodeParameter.Value as string, true);
            }

            return HttpStatusCode.Forbidden;
        }

        public async Task<HttpStatusCode> POSTRequest(RestRequest request)
        {   
            var response = await client.GetResponseAsync(request);
            return WebResponseToHTTPStatusCode(response);
        }
    }
}
