﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using SmartLib.Models;
using SmartLib.RequestManagers;
using SmartLib.Helpers;
using SmartLib.MVVM.Models;
using System.Linq;

namespace SmartLib.Core
{
    public class BookRequests : Requests
    {
        private RestClient client = null;

        public BookRequests(string url)
        {
            client = new RestClient(url);
        }

        public BookRequests(RestClient client)
        {
            this.client = client;
        }

        private static readonly Dictionary<BookIdentifier, string> BookIdentifierToStringDictionary
            = new Dictionary<BookIdentifier, string>()
            {
                { BookIdentifier.Isbn, "isbn" },
                { BookIdentifier.SysNo, "sysno" },
                { BookIdentifier.Barcode, "barcode" }
            };

        private static readonly Dictionary<BookListCategory, string> BookListCategoryToStringDictionary
            = new Dictionary<BookListCategory, string>()
            {
                { BookListCategory.Top, "top" },
                { BookListCategory.News, "news" }
            };

        private static RestRequest CreateBookRequest(BookIdentifier identifierKind, string identifierValue)
        {
            var request = new RestRequest("books?{identifierKind}={identifierValue}&random={random}", Method.GET);
            request.RequestFormat = DataFormat.Json;
            request.AddUrlSegment("identifierKind", BookIdentifierToStringDictionary[identifierKind]);
            request.AddUrlSegment("identifierValue", identifierValue);
            request.AddUrlSegment("random", Guid.NewGuid().ToString());
            return request;
        }

        private static RestRequest CreateBookCopies(string sysno)
        {
            var request = new RestRequest("books/{sysno}/copies", Method.GET);
            request.RequestFormat = DataFormat.Json;
            request.AddUrlSegment("sysno", sysno);
            return request;
        }

        private static RestRequest CreateCheckAvailabilityRequest(string sysno, string[] barcodes)
        {
            var request = new RestRequest("books/{sysno}/check?barcodes={barcodes}&random={random}", Method.GET);
            request.RequestFormat = DataFormat.Json;
            request.AddUrlSegment("sysno", sysno);
            request.AddUrlSegment("barcodes", string.Join(",",barcodes));
            request.AddUrlSegment("random", Guid.NewGuid().ToString());
            return request;
        }

        private static RestRequest CreateSearchBooksRequest(string title, string author, string library = null, uint limit = 10, uint offset = 0)
        {
            var request = new RestRequest("books/search?query={searchQuery}&limit={limit}&offset={offset}", Method.GET);
            request.RequestFormat = DataFormat.Json;            

            if (string.IsNullOrWhiteSpace(title))
            {                
                request.AddUrlSegment("searchQuery", author);
                request.Resource += "&type=author";
            }
            else if (string.IsNullOrWhiteSpace(author))
            {
                request.AddUrlSegment("searchQuery", title);
                request.Resource += "&type=title";
            }
            else
            {
                request.AddUrlSegment("searchQuery", title + " " + author);
            }

            if (!string.IsNullOrWhiteSpace(library) && library != "--")
            {
                request.Resource += "&library=" + library;
            }

            request.AddUrlSegment("limit", limit.ToString());
            request.AddUrlSegment("offset", offset.ToString());
            return request;
        }

        private static RestRequest CreateBookAdditionalDetailsRequest(string sysno)
        {
            var request = new RestRequest("books/{sysno}/details", Method.GET);
            request.RequestFormat = DataFormat.Json;
            request.AddUrlSegment("sysno", sysno);
            return request;
        }

        public static RestRequest CreateBookCheckRequest(string sysno)
        {
            var request = new RestRequest("books/{sysno}/check", Method.GET);
            request.RequestFormat = DataFormat.Json;
            request.AddUrlSegment("sysno", sysno);
            return request;
        }

        private static RestRequest CreateErrorRequest(string uco, string backtrace, string comment = "")
        {
            var request = new RestRequest("report", Method.POST);
            //request.RequestFormat = DataFormat.Json;
            request.AddParameter("uco", uco);
            request.AddParameter("backtrace", backtrace);
            request.AddParameter("text", comment);
            return request;
        }

        private static RestRequest CreateBookListURL(BookListCategory category,
                                                uint limit,
                                                uint offset
                                               )
        {
            var request = new RestRequest("books/list?category={category}", Method.GET);
            request.RequestFormat = DataFormat.Json;
            request.AddUrlSegment("category", BookListCategoryToStringDictionary[category]);
            return request;
        }

        private static RestRequest CreateLibrariesRequest()
        {
            var request = new RestRequest("libraries", Method.GET);
            request.RequestFormat = DataFormat.Json;
            return request;
        }

        public async Task<IRestResponse<List<Book>>> SearchBooks(string title, string author, string library = null, uint limit = 10, uint offset = 0)
        {
            //ArgumentValidator.AssertNotNullOrWhiteSpaceString(title, "title");

            var request = CreateSearchBooksRequest(title, author, library, limit, offset);
            var response = await client.GetResponseAsyncEx<List<Book>>(request);
            return response;
        }

        /// <summary>
        /// Sends request to receive another information about book. (additional information).
        /// </summary>
        /// <param name="book">book to be updated</param>
        /// <returns>Book with additional information.</returns>
        public async Task<Book> GetBookDetails(Book book)
        {
            ArgumentValidator.AssertNotNull(book, "book");

            var request = CreateBookAdditionalDetailsRequest(book.Sysno);
            var response = await client.GetResponseAsyncEx<Book>(request);
            var details = response.Data;

            if (details != null)
            {
                book.Publisher = details.Publisher;
                book.PublishedDate = details.PublishedDate;
                book.Language = details.Language;
                book.PageType = details.PageType;
                book.PageCount = details.PageCount;
                book.previewUrl = details.previewUrl;
            }
            return book;
        }

        /// <summary>
        /// Sends request to receive book according its identifier.
        /// </summary>
        /// <param name="identifierKind">kind of identifier</param>
        /// <param name="identifierValue">value of identifier</param>
        /// <returns>book with specified identifier</returns>
        public async Task<Book> GetBookBy(BookIdentifier identifierKind, string identifierValue)
        {
            ArgumentValidator.AssertNotNullOrWhiteSpaceString(identifierValue, "identifierValue");

            var request = CreateBookRequest(identifierKind, identifierValue);
            var response = await client.GetResponseAsyncEx<Book>(request);
            return response.Data;
        }

        /// <summary>
        /// Sends request to receive books of specified category.
        /// </summary>
        /// <param name="category">books category</param>
        /// <param name="limit">number of results</param>
        /// <param name="offset">number of skipped results</param>
        /// <returns>books of specified category</returns>
        public async Task<IEnumerable<Book>> GetBooksByCategory(BookListCategory category, uint limit, uint offset)
        {
            var request = CreateBookListURL(category, limit, offset);
            var response = await client.GetResponseAsyncEx<List<Book>>(request);
            return response.Data;

        }

        public async Task<IEnumerable<BookCopy>> GetBookCopies(Book book)
        {
            var request = CreateBookCopies(book.Sysno);
            var response = await client.GetResponseAsyncEx<List<BookCopy>>(request);
            return response.Data;

        }

        public async Task<IEnumerable<Availability>> CheckBookAvailability(string sysno, string[] barcodes)
        {
            var request = CreateCheckAvailabilityRequest(sysno, barcodes);
            var response = await client.GetResponseAsyncEx<List<Availability>>(request);
            return response.Data;
        }

        public async Task<IRestResponse> PostError(string uco, string backtrace, string comment = "")
        {
            var request = CreateErrorRequest(uco, backtrace, comment);
            var response = await client.GetResponseAsync(request);
            return response;

        }

        public async Task<IEnumerable<Library>> GetLibraries()
        {
            var request = CreateLibrariesRequest();
            var response = await client.GetResponseAsyncEx<List<Library>>(request);
            return response.Data;

        }

        //public async Task<IRestResponse> CheckAvailability(string sysno, string[] barcodes)
        //{
        //    var request = CreateCheckAvailabilityRequest(sysno, barcodes);
        //    var reponse = await client.GetResponseAsync<(request);
        //    return response.D;
        //}
    }

    //public enum BookIdentifier
    //{
    //    Isbn,
    //    SysNo,
    //    Barcode
    //}

    //public enum BookListCategory
    //{
    //    Top,
    //    News
    //}
}
