﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Threading.Tasks;
using RestSharp;
using System.Linq;
using System.Diagnostics;
using Microsoft.Phone.Controls;
using SmartLib.ViewModels;

namespace SmartLib.RequestManagers
{
    public static class RestClientExtensions
    {
        private static string cookies = null;

        private static Task<T> SelectAsync<T>(this RestClient client, IRestRequest request, Func<IRestResponse, T> selector)
        {
            Debug.WriteLine("Request");
            Debug.WriteLine("==============");
            Debug.WriteLine(request.Resource);
            Debug.WriteLine(request.Parameters);
            Debug.WriteLine("==============");
            var tcs = new TaskCompletionSource<T>();
            if (cookies != null)
            {
                request.AddHeader("Cookie",
                              cookies);
            }
            var loginResponse = client.ExecuteAsync(request, r =>
            {
                if (r != null)
                {
                    var dateHeaders = r.Headers.Where(header => header.Name == "Date");

                    try
                    {
                        if (dateHeaders.Count() > 0)
                        {
                            string serverDateString = dateHeaders.First().Value as string;
                            var serverDateTime = DateTime.Parse(serverDateString);
                            App.CurrentApplication.TimeDifference = serverDateTime - DateTime.Now;

                        }
                    }
                    catch
                    {
                    }

                    if (r.ErrorException == null)
                    {
                        if (r.ResponseUri != null
                             && !string.IsNullOrWhiteSpace(r.ResponseUri.OriginalString)
                            && r.ResponseUri.AbsoluteUri.Contains("login"))
                        {
                            var cookiesHeader = r.Headers.Where(header => header.Name == "Set-Cookie");

                            if (cookiesHeader.Count() > 0)
                            {
                                cookies = cookiesHeader.First().Value as string;
                            }
                        }

                        SetConnectionStatus(r);

                        tcs.SetResult(selector(r));
                    }
                    else
                    {
                        tcs.SetException(r.ErrorException);
                    }
                }
            });
            return tcs.Task;
        }

        public static void SetConnectionStatus(IRestResponse r)
        {
            
            Deployment.Current.Dispatcher.BeginInvoke(()=>
            {
                BaseViewModel viewModel = null;
                viewModel = App.CurrentApplication.RootFrame.DataContext as BaseViewModel;
                if (viewModel != null)
                {
                    viewModel.ConnectionErrorOccured = false;
                }

                if (r.StatusCode == HttpStatusCode.NotFound)
                {
                    if (viewModel != null)
                    {
                        viewModel.ConnectionErrorOccured = true;
                    }
                }
            });
             
            //BaseViewModel viewModel = null;

            //if (currentPage != null)
            //{
            //    viewModel = currentPage.DataContext as BaseViewModel;
            //}
        }

        public static Task<IRestResponse<T>> SelectAsyncEx<T>(this RestClient client, IRestRequest request) where T : new()
        {
            Debug.WriteLine("Request");
            Debug.WriteLine("==============");
            Debug.WriteLine(request.Resource);
            Debug.WriteLine(string.Join( ", ", request.Parameters.Select(par => par.Name + " - " + par.Value + " - " + par.Type)));
            Debug.WriteLine(client.BaseUrl);
            Debug.WriteLine("==============");
            var tcs = new TaskCompletionSource<IRestResponse<T>>();

            if (cookies != null)
            {
                request.AddHeader("Cookie",
                              cookies);
            }

            var loginResponse = client.ExecuteAsync<T>(request, r =>
            {
                if (r != null)
                {//checkni preco to padalo

                    var dateHeaders = r.Headers.Where(header => header.Name == "Date");

                    if (dateHeaders.Count() > 0)
                    {
                        try
                        {
                            string serverDateString = dateHeaders.First().Value as string;
                            var serverDateTime = DateTime.Parse(serverDateString);
                            App.CurrentApplication.TimeDifference = serverDateTime - DateTime.Now;
                        }
                        catch
                        {
                        }

                    }

                    if (r.ErrorException == null)
                    {
                        if (r.ResponseUri != null
                            && !string.IsNullOrWhiteSpace(r.ResponseUri.OriginalString)
                            && r.ResponseUri.AbsoluteUri.Contains("login"))
                        {
                            var cookiesHeader = r.Headers.Where(header => header.Name == "Set-Cookie");

                            if (cookiesHeader.Count() > 0)
                            {
                                cookies = cookiesHeader.First().Value as string;
                            }
                        }

                        SetConnectionStatus(r);

                        tcs.SetResult(r);
                    }
                    else
                    {
                        tcs.SetException(r.ErrorException);
                    }
                }
            });
            return tcs.Task;
        }

        public static Task<string> GetContentAsync(this RestClient client, IRestRequest request)
        {

            return client.SelectAsync(request, r => r.Content);
        }

        public static Task<IRestResponse> GetResponseAsync(this RestClient client, IRestRequest request)
        {
            return client.SelectAsync(request, r => r);
        }

        public static Task<IRestResponse<T>> GetResponseAsyncEx<T>(this RestClient client, IRestRequest request) where T : new()
        {
            return client.SelectAsyncEx<T>(request);
        }

        public static HttpStatusCode WebResponseToHTTPStatusCode(this IRestResponse response)
        {
            if (response == null)
                throw new ArgumentNullException("response");

            var statusCodeParameters = response.Headers.Where(parameter => parameter.Name == "StatusCode");

            if (statusCodeParameters.Count() > 0)
            {
                var statusCodeParameter = statusCodeParameters.First();
                return (HttpStatusCode)Enum.Parse(typeof(HttpStatusCode), statusCodeParameter.Value as string, true);
            }

            return HttpStatusCode.Forbidden;
        }
    }
}
