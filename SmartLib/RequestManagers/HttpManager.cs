﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Threading;
using System.IO;
using System.Linq;
using System.Net.Browser;
using System.Diagnostics;
using System.Collections.Generic;
using SmartLib.Models;
using System.Runtime.Serialization.Json;
using System.Text;

namespace SmartLib.Core
{
    public class HttpManager
    {
        public const string HOST_URL = @"https://web-smartlibweb.rhcloud.com/api/";
        public const int MessageTimeout = 30000;
        public bool LoggedIn { get; private set; }
        private string cookiesHeader = null;

        #region success/error events

        public event EventHandler<Events.SuccessEventArgs> SuccessEvent;

        public event EventHandler<Events.ErrorEventArgs> ErrorEvent;

        public void OnError(string errorMsg)
        {
            var handler = ErrorEvent;

            if (handler != null)
            {
                handler(this, new Events.ErrorEventArgs(errorMsg));
            }
        }

        public void OnSuccess(string successMsg)
        {
            var handler = SuccessEvent;

            if (handler != null)
            {
                handler(this, new Events.SuccessEventArgs(successMsg));
            }
        }

        #endregion

        #region delegates

        public delegate void WriteDataCallback(StreamWriter sw);

        public delegate void SuccessCallback(string data);

        public delegate void BinarySuccessCallback(string contentType, byte[] data);

        public delegate void FinishedCallback();

        public delegate void ErrorCallback(string error);

        public delegate void StatusCodeCallback(HttpStatusCode statusCode);

        #endregion

        public void Login(
            string user,
            string password,
            SuccessCallback scb, 
            ErrorCallback ecb
            )
        {
            Send(
                UriHelper.CreateLogInUri(),
                "POST",
                sw => sw.Write("uco=" + user + "&password=" + password),
                scb,
                null,
                statusCode =>
                {
                    switch (statusCode)
                    {
                        case HttpStatusCode.OK:
                            OnSuccess("You were successfuly logged in.");
                            break;
                        case HttpStatusCode.Unauthorized:
                            OnError("Wrong password.");
                            break;
                        default:
                            Debug.WriteLine("Unxpected Status Code '" + statusCode + "'.");
                            break;
                    }
                }
            );
        }

        public void GetBookList(
            BookListCategory category,
            uint limit,
            uint offset,
            Action<List<Book>> onSuccess
            )
        {
            Send(
                UriHelper.CreateGetBookListUri(category, limit, offset),
                "GET",
                null,
                data =>
                {
                    ParseData <List<Book>>(
                        data,
                        list => onSuccess(list), 
                        error => OnError(error));
                },
                null,
                null
            );
        }

        public void SearchBook(
            string text,
            Action<List<Book>> onSuccess,
            uint limit = 10,
            uint offset = 0)
        {
            Send(
                UriHelper.CreateSearchBookUri(text),
                "GET",
                null,
                data =>
                {
                    ParseData<List<Book>>(
                        data,
                        list => onSuccess(list),
                        error => OnError(error));
                },
                null,
                null
            );
        }

        private void Send(
            string uri,
            string method,
            WriteDataCallback wdcb,
            SuccessCallback scb,
            FinishedCallback fcb,
            StatusCodeCallback sccb
            )
        {
            bool httpResult = HttpWebRequest.RegisterPrefix("https://", WebRequestCreator.ClientHttp);
            var req = WebRequest.CreateHttp(HOST_URL + uri);

            //req.ContentType = "application/x-www-form-urlencoded";
            req.Method = method;

            if (cookiesHeader != null)
            {
                req.Headers["Cookie"] = cookiesHeader;
            }

            var waitHandle = new ManualResetEvent(false);

            req.BeginGetRequestStream(ar =>
            {
                try
                {
                    if (wdcb != null)
                    {
                        using (var requestStream = req.EndGetRequestStream(ar))
                        {
                            using (var sr = new StreamWriter(requestStream))
                            {
                                wdcb(sr);
                            }
                        }
                    }

                    req.BeginGetResponse(
                        a =>
                        {
                            waitHandle.Set();

                            try
                            {
                                var response = (HttpWebResponse)req.EndGetResponse(a);

                                var responseStream = response.GetResponseStream();

                                using (var sr = new StreamReader(responseStream))
                                {
                                    if (scb != null)
                                    {
                                        scb(sr.ReadToEnd());
                                    }

                                    if (fcb != null)
                                    {
                                        fcb();
                                    }
                                }

                                if (response.Headers.AllKeys.Contains("Set-Cookie"))
                                {
                                    cookiesHeader = response.Headers["Set-Cookie"];
                                }

                                if (sccb != null)
                                {
                                    sccb(WebResponseToHTTPStatusCode(response));
                                }
                            }
                            catch (WebException e)
                            {
                                if (e.Status == WebExceptionStatus.RequestCanceled)
                                {
                                    //OnError("");
                                    return;
                                }

                                var response = (HttpWebResponse)e.Response;

                                if (response.StatusCode == HttpStatusCode.Forbidden)
                                {
                                    LoggedIn = false;
                                }

                                try
                                {
                                    using (var responseStream = response.GetResponseStream())
                                    {
                                        using (var sr = new StreamReader(responseStream))
                                        {
                                            OnError(sr.ReadToEnd());
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    // What is wrong with this platform?!
                                    OnError(ex.Message + "\n" + e.Message);
                                }
                            }
                        }, null
                    );
                }
                catch (WebException)
                {
                    // The request was aborted
                    OnError("Server Error");
                }
            }, null);

            ThreadPool.QueueUserWorkItem(
                state =>
                {
                    if (!waitHandle.WaitOne(MessageTimeout))
                    {
                        (state as HttpWebRequest).Abort();
                    }
                },
                req
            );
        }

        private static HttpStatusCode WebResponseToHTTPStatusCode(WebResponse response)
        {
            if (response == null)
                throw new ArgumentNullException("response");

            if (response.Headers.AllKeys.Contains("StatusCode"))
            {
                return (HttpStatusCode)Enum.Parse(typeof(HttpStatusCode), response.Headers["StatusCode"], true);
            }

            return HttpStatusCode.Forbidden;
        }

        public static void ParseData<T>(string json, Action<T> onSuccess, Action<string> onError)
        {

            T data = default(T);
            try
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
                data = (T)serializer.ReadObject(ms);
                onSuccess(data);
            }
            catch (Exception ex)
            {
                onError(ex.Message);
            }
        }
    }
}
