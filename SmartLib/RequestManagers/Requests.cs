﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SmartLib.Core
{
    public class Requests : ILogable
    {
        public event EventHandler<Events.SuccessEventArgs> SuccessEvent;

        public event EventHandler<Events.ErrorEventArgs> ErrorEvent;

        public void OnError(string errorMsg)
        {
            var handler = ErrorEvent;

            if (handler != null)
            {
                handler(this, new Events.ErrorEventArgs(errorMsg));
            }
        }

        public void OnSuccess(string successMsg)
        {
            var handler = SuccessEvent;

            if (handler != null)
            {
                handler(this, new Events.SuccessEventArgs(successMsg));
            }
        }
    }
}
