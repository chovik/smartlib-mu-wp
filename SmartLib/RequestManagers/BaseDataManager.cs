﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SmartLib.Helpers;
using SmartLib.RequestManagers;

namespace SmartLib.DataManagers
{
    public class BaseDataManager
    {

        public event EventHandler<MessageEventArgs> ErrorOccured;
        public event EventHandler<MessageEventArgs> ActionSuccessfullyDone;

        /// <summary>
        /// Vola pri chybe robota. Zasignalizuje chybu prostrednictvom event.
        /// </summary>
        /// <param name="e">obsahuje informacie o chybe.</param>
        protected virtual void OnError(string message)
        {
            var errorEvent = ErrorOccured;
            if (errorEvent != null)
                errorEvent(this, new MessageEventArgs(message));
        }

        /// <summary>
        /// Vola sa, ked robot vykona nejaku cinnost. Zasignalizuje zmenu prostrednictvom event.
        /// </summary>
        /// <param name="e">obsahuje informacie o prave vykonanej cinnosti</param>
        protected virtual void OnSuccess(string message)
        {
            var actionEvent = ActionSuccessfullyDone;
            if (actionEvent != null)
                actionEvent(this, new MessageEventArgs(message));
        }

        private string serverAddress;

        /// <summary>
        /// Address of SmartLib server.
        /// </summary>
        public string ServerAddress
        {
            get { return serverAddress; }
            set 
            {
                ArgumentValidator.AssertNotNull(value, "server");
                ArgumentValidator.AssertNotNullOrWhiteSpaceString(value, "server");
                serverAddress = value; 
            }
        }
        

        private RequestManager requestManager;

        /// <summary>
        /// Manager used tos end/receive data from/to server.
        /// </summary>
        public RequestManager RequestManager 
        {
            get
            {
                return requestManager;
            }
            set
            {
                ArgumentValidator.AssertNotNull(value, "requestManager");
                requestManager = value;
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="requestManager">manager used to send/receive data from/to server</param>
        /// <param name="serverAddress">address of SmartLib server</param>
        public BaseDataManager(RequestManager requestManager, string serverAddress)
        {
            RequestManager = requestManager;
            ServerAddress = serverAddress;
        }
    }
}
