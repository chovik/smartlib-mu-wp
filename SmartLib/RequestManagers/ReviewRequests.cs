﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using RestSharp;
using System.Threading.Tasks;
using System.Collections.Generic;
using SmartLib.Models;
using System.Diagnostics;
using SmartLib.RequestManagers;
using System.Globalization;
using Microsoft.Phone.Info;
using System.Linq;

namespace SmartLib.Core
{
    public class ReviewRequests : Requests
    {
        private RestClient client = null;

        public ReviewRequests(string url)
        {
            client = new RestClient(url);
        }

        public ReviewRequests(RestClient client)
        {
            this.client = client;
        }

        public RestRequest CreatePostRatingRequest(string sysno, double rating)
        {
            var request = new RestRequest("books/{sysno}/reviews", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddUrlSegment("sysno", sysno);
            object deviceId = "";
            DeviceExtendedProperties.TryGetValue("DeviceUniqueId", out deviceId);
            string deviceIdString = string.Join("", deviceId as byte[]);
            request.AddParameter("device_id", deviceIdString);
            request.AddParameter("rating", rating);
            return request;
        }

        public RestRequest CreatePostReviewRequest(string sysno, string review, uint rating)
        {
            var request = new RestRequest("books/{sysno}/reviews", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddUrlSegment("sysno", sysno);
            request.AddParameter("text", review);
            request.AddParameter("rating", rating);
            return request;
        }

        private RestRequest CreateGetBookReviewsRequest(string sysno, uint limit, uint offset)
        {
            var request = new RestRequest("books/{sysno}/reviews?limit={limit}&offset={offset}&random={random}", Method.GET);
            request.RequestFormat = DataFormat.Json;
            request.AddUrlSegment("sysno", sysno);
            request.AddUrlSegment("limit", limit.ToString());
            request.AddUrlSegment("offset", offset.ToString());
            request.AddUrlSegment("random", Guid.NewGuid().ToString());
             
            return request;
        }

        private RestRequest CreateGetBookRatingsRequest(string sysno)
        {
            var request = new RestRequest("books/{sysno}/ratings?random={random}", Method.GET);
            request.RequestFormat = DataFormat.Json;
            request.AddUrlSegment("sysno", sysno);
            request.AddUrlSegment("random", Guid.NewGuid().ToString());
            return request;
        }

        public async Task<HttpStatusCode> SaveReview(string sysno, string review, uint rating)
        {
            if (sysno == null)
                throw new ArgumentNullException("sysno");
            if (string.IsNullOrWhiteSpace(sysno))
                throw new ArgumentException("sysno");

            if (review == null)
                throw new ArgumentNullException("review");
            if (string.IsNullOrWhiteSpace(review))
                throw new ArgumentException("review");

            if (rating < 0 || rating > 5)
                throw new ArgumentException("rating");

            HttpStatusCode statusCode = HttpStatusCode.NotFound;

            try
            {
                var request = CreatePostReviewRequest(sysno, review, rating);
                var response = await client.GetResponseAsync(request);
                statusCode = response.WebResponseToHTTPStatusCode();

                //process returned status code (from server response)
                switch (statusCode)
                {
                    case HttpStatusCode.OK:
                        OnSuccess("Review has been saved.");
                        break;
                    case HttpStatusCode.Unauthorized:
                        OnError("Need authorization.");
                        App.CurrentApplication.LoggedIn = false;
                        App.CurrentApplication.LoggedUco = null;
                        break;
                    default:
                        App.CurrentApplication.LoggedIn = false;
                        App.CurrentApplication.LoggedUco = null;
                        Debug.WriteLine("Review - Unxpected Status Code '{0}'.", statusCode);
                        break;
                }
            }
            catch (WebException ex)
            {
                OnError("The server is not responding , please check your connection and try again.");
            }

            return statusCode;            
        }


        public async Task<HttpStatusCode> RateBook(string sysno, double rating)
        {
            if (sysno == null)
                throw new ArgumentNullException("sysno");
            if (string.IsNullOrWhiteSpace(sysno))
                throw new ArgumentException("sysno");

            if (rating < 0 || rating > 5)
                throw new ArgumentException("rating");

            HttpStatusCode statusCode = HttpStatusCode.NotFound;

            try
            {
                var request = CreatePostRatingRequest(sysno, rating);
                var response = await client.GetResponseAsync(request);
                statusCode = response.WebResponseToHTTPStatusCode();

                //process returned status code (from server response)
                //switch (statusCode)
                //{
                //    case HttpStatusCode.OK:
                //        OnSuccess("Review has been saved.");
                //        break;
                //    case HttpStatusCode.Unauthorized:
                //        OnError("Need authorization.");
                //        App.CurrentApplication.LoggedIn = false;
                //        App.CurrentApplication.LoggedUco = null;
                //        break;
                //    default:
                //        App.CurrentApplication.LoggedIn = false;
                //        App.CurrentApplication.LoggedUco = null;
                //        Debug.WriteLine("Review - Unxpected Status Code '{0}'.", statusCode);
                //        break;
                //}
            }
            catch (WebException ex)
            {
                OnError("The server is not responding , please check your connection and try again.");
            }

            return statusCode;
        }

        public async Task<IEnumerable<Review>> GetBookReviews(string sysno, uint limit, uint offset)
        {
            if (sysno == null)
                throw new ArgumentNullException("sysno");
            if (string.IsNullOrWhiteSpace(sysno))
                throw new ArgumentException("sysno");

            var request = CreateGetBookReviewsRequest(sysno, limit, offset);

            var response = await client.SelectAsyncEx<List<Review>>(request);
            return response.Data;
        }

        public async Task<Dictionary<double, int>> GetBookRatings(string sysno)
        {
            if (sysno == null)
                throw new ArgumentNullException("sysno");
            if (string.IsNullOrWhiteSpace(sysno))
                throw new ArgumentException("sysno");

            var request = CreateGetBookRatingsRequest(sysno);

            var response = await client.SelectAsyncEx<Dictionary<string, int>>(request);
            Dictionary<double, int> castedDictionary = new Dictionary<double,int>();
            
            if (response.Data != null)
            {
                foreach (var rating in response.Data)
                {
                    var castedKey = double.Parse(rating.Key, CultureInfo.InvariantCulture);
                    if(castedDictionary.ContainsKey(castedKey))
                    {
                        castedDictionary[castedKey] += rating.Value;
                    }
                    else
                    {
                        castedDictionary.Add(castedKey, rating.Value);
                    }
                }
            }

            return castedDictionary;
        }


    }
}
