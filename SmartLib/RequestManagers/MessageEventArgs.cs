﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SmartLib.RequestManagers
{
    public class MessageEventArgs : EventArgs
    {
        public string Message { get; private set; }

        public MessageEventArgs(string message)
        {
            if (message == null)
                throw new ArgumentNullException("message");
            if (string.IsNullOrWhiteSpace(message))
                throw new ArgumentException("message");

            this.Message = message;
        }
    }
}
