﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace SmartLib.Core
{
    public static class UriHelper
    {
        #region book uri

        private static readonly Dictionary<BookIdentifier, string> BookIdentifierToStringDictionary
            = new Dictionary<BookIdentifier, string>()
            {
                { BookIdentifier.Isbn, "isbn" },
                { BookIdentifier.SysNo, "sysno" },
                { BookIdentifier.Barcode, "barcode" }
            };

        private static readonly Dictionary<BookListCategory, string> BookListCategoryToStringDictionary
            = new Dictionary<BookListCategory, string>()
            {
                { BookListCategory.Top, "top" },
                { BookListCategory.News, "news" }
            };

        public static string CreateGetBookUri(
            BookIdentifier identifierKind, string identifierValue)
        {
            return "books?"
            + BookIdentifierToStringDictionary[identifierKind]
            + "="
            + identifierValue;
        }

        public static string CreateSearchBookUri(
            string searchQuery, uint limit = 10, uint offset = 0)
        {
            return "books/search?query="
            + searchQuery
            + "&limit="
            + limit
            + "&offset="
            + offset;
        }

        public static string CreateGetBookAdditionalDetailsUri(string sysno)
        {
            return "books/"
            + sysno
            + "/details";
        }

        public static string CreateGetBookListUri(
            BookListCategory category, uint limit, uint offset)
        {
            return "books/list?category="
            + category;
        }

        #endregion

        #region review uri

        public static string CreatePostReviewUri(string sysno)
        {
            return "books/"
            + sysno
            + "/reviews";
        }

        public static string CreateGetBookReviewsUri(string sysno, uint limit, uint offset)
        {
            return "books/"
            + sysno
            + "/reviews?limit="
            + limit 
            + "&offset="
            + offset;
        }

        public static string CreateGetBookRatingsRequest(string sysno)
        {
            return "books/"
            + sysno
            + "/ratings";
        }

        #endregion

        #region user uri

        public static string CreateCheckAuthenticationUri()
        {
            return "user/authentication";
        }

        public static string CreateLogInUri()
        {
            return "user/login";
        }

        public static string CreateChangePasswordRequest()
        {
            return "user/changepassword";
        }

        public static string CreateSignUpRequest(string uco, string firstName, string lastName)
        {
            return "user/registration";
        }

        public static string CreateLogOutRequest()
        {
            return "user/logout";
        }

        #endregion
    }
}
