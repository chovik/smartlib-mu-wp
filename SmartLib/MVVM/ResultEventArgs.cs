﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZXing;

namespace SmartLib.MVVM
{
    public class ResultEventArgs : EventArgs
    {
        public Result Result { get; set; }
    }
}
