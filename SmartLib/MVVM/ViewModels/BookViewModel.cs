﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SmartLib.Models;
using System.Collections.ObjectModel;
using SmartLib.Helpers;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using SmartLib.MVVM.Models;
using Microsoft.Phone.Tasks;
using System.Threading;
using System.Windows.Threading;
using SmartLib.MVVM.ViewModels;

namespace SmartLib.ViewModels
{
    [DataContract]
    public class BookViewModel : BaseViewModel
    {
        const string transientStateKey = "BookViewModel_Book";

        private Book book;
        /// <summary>
        /// Book.
        /// </summary>
        [DataMember]
        public Book Book
        {
            get
            {
                return book;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("book");

                book = value;
                OnNotifyPropertyChanged("Book");
            }
        }

        private ObservableCollection<ReviewViewModel> reviews = new ObservableCollection<ReviewViewModel>();

        /// <summary>
        /// Book Reviews.
        /// </summary>
        [DataMember]
        public ObservableCollection<ReviewViewModel> Reviews
        {
            get
            {
                return reviews;
            }
            set
            {
                if (value != reviews)
                {
                    reviews = value;
                    OnNotifyPropertyChanged("Reviews");
                }
            }
        }

        private ObservableCollection<BookCopyViewModel> copies;

        [DataMember]
        public ObservableCollection<BookCopyViewModel> Copies
        {
            get
            {
                if (copies == null)
                {
                    copies = new ObservableCollection<BookCopyViewModel>();
                }

                return copies;
            }

        }

        public event EventHandler IsBookmarkedChanged;

        public void OnIsBookmarkedChanged()
        {
            var handler = IsBookmarkedChanged;

            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        //private bool isBookmarked = false;

        /// <summary>
        /// Gets or sets a value indicating whether the book is favourite or not.
        /// True if book is favourite. Otherwise false.
        /// </summary>
        private bool isBookmarked = false;
        public bool IsBookmarked
        {
            get
            {
                return isBookmarked;
            }

            set
            {
                if (value != isBookmarked)
                {
                    isBookmarked = value;
                    OnNotifyPropertyChanged("IsBookmarked");
                    OnIsBookmarkedChanged();
                }
            }
            //get 
            //{ 
            //    var favourites = App.CurrentApplication.FavouriteBooks;
            //    return favourites.Contains(this);
            //}
            //set 
            //{
            //    if (value != isBookmarked)
            //    {
            //        isBookmarked = value;
            //        OnNotifyPropertyChanged("IsBookmarked");
            //        App.CurrentApplication.News
            //    }
            //}
        }

        private bool toRemoveFromFavourites = false;
        public bool ToRemoveFromFavourites
        {
            get
            {
                return toRemoveFromFavourites;
            }
            set
            {
                if (value != toRemoveFromFavourites)
                {
                    toRemoveFromFavourites = value;
                    OnNotifyPropertyChanged("ToRemoveFromFavourites");
                }
            }
        }

        /// <summary>
        /// Checks cover existence. If server does not return cover, the default cover will be set;
        /// </summary>
        public void CheckCoverExistence()
        {
            //determine if cover was load
            if (string.IsNullOrWhiteSpace(this.Book.coverUrl))
            {
                //set default cover
                //var uri = new Uri(".", UriKind.Relative);
                this.Book.coverUrl = "/Images/no_cover_thumb.png";
            }
        }
	

        /// <summary>
        /// Gets formatted string of book authors.
        /// </summary>
		public string Authors
        {
            get
            {
                string authorsString = "no authors";

                if (book != null
                    && book.Authors != null
                    && book.Authors.Count > 0)
                {
                    authorsString = string.Join(", ", book.Authors);
                }
                return authorsString;
            }
        }

        private RelayCommand addReviewCommand;

        /// <summary>
        /// If command is executed, GoToAddReviewPage method will be called.
        /// </summary>
        public RelayCommand AddReviewCommand
        {
            get
            {
                if (addReviewCommand == null) // RelayCommand nieje serializovatelny a pri nacitani historie a oblubenych knih sposobovalo chybu
                    addReviewCommand = new RelayCommand(() => GoToAddReviewPage());

                return addReviewCommand;
            }
            private set
            {
                if (value != addReviewCommand)
                    addReviewCommand = value;
            }
        }

        private RelayCommand reloadCommand;

        /// <summary>
        /// If command is executed, GoToAddReviewPage method will be called.
        /// </summary>
        public RelayCommand ReloadCommand
        {
            get
            {
                if (reloadCommand == null) // RelayCommand nieje serializovatelny a pri nacitani historie a oblubenych knih sposobovalo chybu
                    reloadCommand = new RelayCommand(() => ReloadData());

                return reloadCommand;
            }
            private set
            {
                if (value != reloadCommand)
                    reloadCommand = value;
            }
        }

        private RelayCommand bookmarkCommand;

        /// <summary>
        /// If command is executed, AddToFavourites method will be called.
        /// </summary>
        public RelayCommand BookmarkCommand
        {
            get
            {
                if (bookmarkCommand == null) // RelayCommand nieje serializovatelny a pri nacitani historie a oblubenych knih sposobovalo chybu
                    bookmarkCommand = new RelayCommand(() => Bookmark());

                return bookmarkCommand;
            }
            private set
            {
                if (value != bookmarkCommand)
                    bookmarkCommand = value;
            }
        }

        private RelayCommand previewCommand;

        /// <summary>
        /// If command is executed, AddToFavourites method will be called.
        /// </summary>
        public RelayCommand PreviewCommand
        {
            get
            {
                if (previewCommand == null) // RelayCommand nieje serializovatelny a pri nacitani historie a oblubenych knih sposobovalo chybu
                    previewCommand = new RelayCommand(() => OpenPreview());

                return previewCommand;
            }
            private set
            {
                if (value != previewCommand)
                    previewCommand = value;
            }
        }

        /// <summary>
        /// Number of votes.
        /// </summary>
        public uint RatingCount
        {
            get { return this.Book.RatingCount; }
            set
            {
                if (value != this.Book.RatingCount)
                {
                    this.Book.RatingCount = value;
                    OnNotifyPropertyChanged("RatingCount");
                }
            }
        }

        /// <summary>
        /// Average rating.
        /// </summary>
        public double Rating
        {
            get { return this.Book.averageRating * 2; }
            set
            {
                //if (value != this.Book.averageRating)
                //{
                    value = value / 2;
                    this.Book.averageRating = value;
                    OnNotifyPropertyChanged("Rating");
                    if (RatingTimer != null && Loaded
                        && updatingRating == 0)
                    {
                        if (RatingTimer.IsEnabled)
                        {
                            RatingTimer.Stop();
                        }
                        RatingTimer.Start();
                    }
                //}
            }
        }

        private DispatcherTimer ratingTimer;

        public DispatcherTimer RatingTimer
        {
            get 
            {
                if (ratingTimer == null)
                {
                    ratingTimer = new DispatcherTimer();
                    ratingTimer.Interval = TimeSpan.FromMilliseconds(1000);
                    ratingTimer.Tick += async (s, e) =>
                    {
                        ratingTimer.Stop();
                        Loaded = false;
                        await App.CurrentApplication.ReviewRequests.RateBook(this.Book.Sysno, Rating / 2.0);
                        UpdateRating();
                    };
                }
                return ratingTimer; 
            }
        }

        //private int loadedMonitor = 0;

        public async void ReloadData()
        {
            UpdateBookDetails();
            UpdateRating();
            UpdateReviews();
            UpdateCopies();
        }

        public static double RoundToHalf(double numberToRound)
        {

            double remainder = numberToRound - Math.Floor(numberToRound);
            if(remainder >= 0.3d && remainder <= 0.7d)
            {
                return Math.Floor(numberToRound) + 0.5d;
            }
            else if(remainder > 0.7)
                return Math.Ceiling(numberToRound);
            return Math.Floor(numberToRound);
        }

        private int updatingRating = 0;

        /// <summary>
        /// Updates Rating and RatingCount value.
        /// </summary>
        public virtual async void UpdateRating()
        {
            try
            {
                // Debug.Assert(this.Book.Sysno != "000041143");
                ConnectionErrorOccured = false;
                Loaded = false;
                updatingRating++;
                //loadedMonitor++;
                Debug.WriteLine("Rating updating");
                //send request "get ratings" to server
                
                    //if (Book.Sysno != null)
                    //{
                var ratings = await App.CurrentApplication.ReviewRequests.GetBookRatings(this.Book.Sysno);
                try
                {
                    Debug.WriteLine("Rating updated");
                    if (ratings != null
                        && ratings.Count() > 0)
                    {
                        double average = 0;

                        //int i = 1;
                        int ratingCount = 0;
                        foreach (var rating in ratings)
                        {
                            ratingCount += rating.Value;
                            average += rating.Key * rating.Value;
                            //i++;
                        }

                        if (ratingCount > 0)
                        {
                            average = average / ratingCount;
                            average = RoundToHalf(average) * 2;
                        }

                        this.Rating = (uint)average;
                        this.RatingCount = (uint)ratingCount;
                    }
                    //}
                }
                catch(Exception ex2)
                {
                    Debug.WriteLine("rating exception");
                }
            }
            catch (WebException ex)
            {
                //App.CurrentApplication.MessageService.ShowErrorMessage("The server is not responding , please check your connection and try again.", "Connection Error");
                ConnectionErrorOccured = true;
            }
            finally
            {
                Loaded = true;
                updatingRating--;
            }
        }


        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="book">book to wrap</param>
        public BookViewModel(Book book)
        {
            Book = book;
            ConnectionErrorTitle = "Updating Book Details";
        }


        /// <summary>
        /// Adds/removes book from favourites.
        /// </summary>
        public void Bookmark()
        {
            var favourites = BookManager.Instance.FavouriteBooks;
            //IsBookmarked = !favourites.Contains(this);
            if (IsBookmarked)
            {
                favourites.Remove(this);
                App.CurrentApplication.MessageService.ShowSuccessMessage(Localization.fav_removed, Localization.favourites);
            }
            else
            {
                favourites.Add(this);
                App.CurrentApplication.MessageService.ShowSuccessMessage(Localization.fav_added, Localization.favourites);
            }

            OnNotifyPropertyChanged("IsBookmarked");
        }

        /// <summary>
        /// Navigates to AddReviewPage or to LoginPage.
        /// If user is already logged in, it navigates to AddReviewPage. Otherwise to LoginPage.
        /// </summary>
        public void GoToAddReviewPage()
        {
            if (App.CurrentApplication.LoggedIn)
            {
                var root = Application.Current.RootVisual as Frame;
                root.Navigate(new Uri(string.Format("/MVVM/View/ReviewPage.xaml?sysno={0}", this.Book.Sysno), UriKind.Relative));
            }
            else
            {
                var root = Application.Current.RootVisual as Frame;
                root.Navigate(new Uri(string.Format("/MVVM/View/LoginPage.xaml?nextPageURI=/MVVM/View/ReviewPage.xaml?sysno={0}", this.Book.Sysno), UriKind.Relative));
            }
        }

        /// <summary>
        /// Updates book details. It is used to update information, that has not been received by searching.
        /// </summary>
        public async void UpdateBookDetails()
        {
            try
            {
                ConnectionErrorOccured = false;
                await BookManager.Instance.GetDetailedBook(this.Book.Sysno);
            }
            catch (WebException webEx)
            {
                ConnectionErrorOccured = true;
            }
            finally
            {
            }
        }

        public void OpenPreview()
        {
            //if (Uri.IsWellFormedUriString(e.Uri))
            //{
                //e.Cancel = true;
            if (Book != null
                && !string.IsNullOrWhiteSpace(Book.previewUrl))
            {
                WebBrowserTask task = new WebBrowserTask();
                task.URL = Book.previewUrl;
                task.Show();
            }
            else
            {
                App.CurrentApplication.MessageService.ShowWarningMessage(Localization.preview_error, Localization.preview);
            }
            //}
        }

        private IEnumerable<BookCopy> allCopiesFromServes = null;

        public async void UpdateCopies()
        {
            try
            {
                ConnectionErrorOccured = false;
                allCopiesFromServes = await App.CurrentApplication.BookRequests.GetBookCopies(this.Book);

                BindCopies();

                //OnNotifyPropertyChanged("Copies");
            }
            catch (WebException webEx)
            {
                ConnectionErrorOccured = true;
            }
            finally
            {
            }
        
        }


        public void BindCopies()
        {
            Copies.Clear();

            Dictionary<string, Dictionary<string, List<BookCopy>>> faculty_type_books = new Dictionary<string, Dictionary<string, List<BookCopy>>>();

            if (allCopiesFromServes != null)
            {
                foreach (var copy in allCopiesFromServes)
                {
                    if (!faculty_type_books.ContainsKey(copy.library ?? ""))
                    {
                        faculty_type_books.Add(copy.library ?? "", new Dictionary<string, List<BookCopy>>());
                    }

                    if (!faculty_type_books[copy.library ?? ""].ContainsKey(copy.type ?? ""))
                    {
                        faculty_type_books[copy.library ?? ""].Add(copy.type ?? "", new List<BookCopy>());
                    }

                    faculty_type_books[copy.library ?? ""][copy.type ?? ""].Add(copy);

                    //Copies.Add(copy);

                    //Copies = Copies;
                }
            }

            foreach (var faculty in faculty_type_books.Keys)
            {
                foreach (var type in faculty_type_books[faculty].Keys)
                {
                    var numberOfAvailable = faculty_type_books[faculty][type].Where(book => book.Status).Count();
                    var numberOfAllCopies = faculty_type_books[faculty][type].Count;
                    Copies.Add(new BookCopyViewModel()
                    {
                        Copy = faculty_type_books[faculty][type].First(),
                        NumberOfAvailableCopies = numberOfAvailable,
                        NumberOfAllCopies = numberOfAllCopies
                    });

                }
            }
        }

        public async void CheckCurrentAvailability()
        {
            if (allCopiesFromServes != null)
            {
                try
                {
                    ConnectionErrorOccured = false;
                    Loaded = false;
                    var avaibilities = await App.CurrentApplication.BookRequests
                        .CheckBookAvailability(Book.Sysno, allCopiesFromServes.Where(copy => copy != null && copy.BarCode != null)
                        .Select(copy => copy.BarCode).ToArray());

                    if (avaibilities != null)
                    {
                        foreach (var availability in avaibilities)
                        {
                            foreach (var bookCopy in allCopiesFromServes.Where(copy => copy.BarCode == availability.Barcode))
                            {
                                bookCopy.Status = availability.Status;
                                bookCopy.Return = availability.Return;
                            }
                        }
                    }

                    BindCopies();

                }
                catch (WebException ex)
                {
                    //App.CurrentApplication.MessageService.ShowErrorMessage("The server is not responding , please check your connection and try again.", "Connection Error");
                    ConnectionErrorOccured = true;
                }
                finally
                {
                    Loaded = true;
                }
            }
        }

        /// <summary>
        /// Updates Book Reviews.
        /// </summary>
        public async void UpdateReviews()
        {
            Reviews.Clear();

            try
            {
                ConnectionErrorOccured = false;

                IEnumerable<Review> newReviews = null;

                newReviews = await App.CurrentApplication.ReviewRequests.GetBookReviews(this.Book.Sysno, 10, 0);

                if (newReviews != null)
                {
                    foreach (var review in newReviews)
                    {
                        Reviews.Add(new ReviewViewModel(review));
                    }
                }
            }
            catch (WebException webEx)
            {
                ConnectionErrorOccured = true;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Adds Book to last viewed books. If Book is already in the list, 
        /// it will be inserted to the beginning of the list.
        /// </summary>
        public void AddToHistory()
        {
            BookManager.Instance.AddBookToHistory(this);
        }

        public void ShareBook()
        {
            ShareLinkTask shareLinkTask = new ShareLinkTask();
            shareLinkTask.LinkUri = new Uri("https://aleph.muni.cz/F?func=find-b&find_code=SYS&request=" + Book.Sysno + "&format=999", UriKind.Absolute);
            shareLinkTask.Title = "Check out " + (Book.Title ?? "No Title") + ": " + Authors + " at SmartLib";
            shareLinkTask.Message = "";
            shareLinkTask.Show();
        }
    }
}