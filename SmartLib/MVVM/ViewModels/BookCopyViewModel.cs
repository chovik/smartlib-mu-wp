﻿using SmartLib.MVVM.Models;
using SmartLib.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartLib.MVVM.ViewModels
{
    public class BookCopyViewModel : BaseViewModel
    {
        private BookCopy copy;

        public BookCopy Copy
        {
            get { return copy; }
            set 
            {
                if (copy != value)
                {
                    copy = value;
                    OnNotifyPropertyChanged("Copy");
                }
            }
        }

        private int numberOfAvailableCopies;

        public int NumberOfAvailableCopies
        {
            get { return numberOfAvailableCopies; }
            set 
            {
                if (numberOfAvailableCopies != value)
                {
                    numberOfAvailableCopies = value;
                    OnNotifyPropertyChanged("NumberOfAvailableCopies");
                }
            }
        }

        private int numberOfAllCopies;

        public int NumberOfAllCopies
        {
            get { return numberOfAllCopies; }
            set
            {
                if (numberOfAllCopies != value)
                {
                    numberOfAllCopies = value;
                    OnNotifyPropertyChanged("NumberOfUnavailableCopies");
                }
            }
        }
        
    }
}
