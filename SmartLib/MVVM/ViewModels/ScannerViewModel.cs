﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Devices;
using System.Diagnostics;
using System.Threading;
using WP7_Barcode_Library;
using System.Windows.Media.Imaging;
using System.IO;
using ZXing.QrCode;
using ZXing.OneD;
using ZXing;
using ZXing.Common;
using System.ComponentModel;
using Microsoft.Phone.Tasks;
using SmartLib.MVVM;
using WindowsPhoneDemo;
using SmartLib.Models;
using System.Collections.Generic;
using Microsoft.Phone.Controls;

namespace SmartLib.ViewModels
{
    public class ScannerViewModel : BaseViewModel
    {
        //private Timer timer;
        //private PhotoCameraLuminanceSource luminance;
        private QRCodeReader qrReader;
        private Code39Reader code39Reader;
        private EAN13Reader ean13Reader;
        private PhotoCamera photoCamera = null;

        public PhotoCamera PhotoCamera
        {
            get
            {
                return photoCamera;
            }
            set
            {
                if (value != photoCamera)
                {
                    photoCamera = value;
                    OnNotifyPropertyChanged("PhotoCamera");
                }
            }
        }

        private IBarcodeReader reader;
        private Timer timer;
        private PhotoCameraLuminanceSource luminance;
        private readonly BackgroundWorker scannerWorker;
        private readonly WriteableBitmap dummyBitmap = new WriteableBitmap(1, 1);
        public PageOrientation Orientation { get; set; }

        public ScannerViewModel()
        {
            scannerWorker = new BackgroundWorker();
            scannerWorker.DoWork += scannerWorker_DoWork;
            scannerWorker.RunWorkerCompleted += scannerWorker_RunWorkerCompleted;
            
        }

        void scannerWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // processing the result of the background scanning
            if (e.Cancelled)
            {
                //BarcodeContent.Text = "Cancelled.";
            }
            else if (e.Error != null)
            {
                //BarcodeContent.Text = e.Error.Message;
            }
            else
            {
                var result = (Result)e.Result;
                OnResultFound(result);
            }
        }

        static void scannerWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // scanning for a barcode
            var barcodeReader = new BarcodeReader() 
            { PossibleFormats = { BarcodeFormat.CODE_39, BarcodeFormat.QR_CODE, BarcodeFormat.EAN_13 } }; ;
            e.Result = barcodeReader.Decode((WriteableBitmap)e.Argument);
        }

        public void StartDetecting()
        {
            if (PhotoCamera == null)
            {
                PhotoCamera = new PhotoCamera();
                PhotoCamera.Initialized += OnPhotoCameraInitialized;
                //previewVideo.SetSource(photoCamera);

                //CameraButtons.ShutterKeyHalfPressed += (o, arg) =>
                //    {
                //        photoCamera.Focus();
                //    };
            }
        }

        public void StopDetecting()
        {
            if (timer != null)
            {
                timer.Dispose();
                timer = null;
            }
            PhotoCamera = null;
        }

        private void ProcessImage(PhotoResult e)
        {
            // setting the image in the display and start scanning in the background
            var bmp = new BitmapImage();
            bmp.SetSource(e.ChosenPhoto);
            //BarcodeImage.Source = bmp;
            scannerWorker.RunWorkerAsync(new WriteableBitmap(bmp));
        }

        private void OnPhotoCameraInitialized(object sender, CameraOperationCompletedEventArgs e)
        {
            var width = Convert.ToInt32(photoCamera.PreviewResolution.Width);
            var height = Convert.ToInt32(photoCamera.PreviewResolution.Height);

            //Dispatcher.BeginInvoke(() =>
            //{
                //previewTransform.Rotation = photoCamera.Orientation;
                // create a luminance source which gets its values directly from the camera
                // the instance is returned directly to the reader
                luminance = new PhotoCameraLuminanceSource(width, height);
                var types = new Dictionary<DecodeHintType, object>() 
                { 
                    {DecodeHintType.POSSIBLE_FORMATS, new List<BarcodeFormat>() { BarcodeFormat.All_1D }},
                    {DecodeHintType.TRY_HARDER, true }
                };
                reader = new BarcodeReader(new MultiFormatOneDReader(types), bmp => luminance, null);
                //reader.PossibleFormats = new List<BarcodeFormat>() { BarcodeFormat.CODE_39, BarcodeFormat.QR_CODE, BarcodeFormat.EAN_13 };
            //});

                PhotoCamera.FlashMode = FlashMode.Off;
                if (timer == null)
                {
                    
                    if (photoCamera.IsFocusSupported)
                    {
                        photoCamera.AutoFocusCompleted += (o, arg) => { if (arg.Succeeded) ScanPreviewBuffer(); };
                        timer = new Timer((t) => { try { photoCamera.Focus(); } catch (Exception) { } }, null, 0, 1500);
                    }
                    else
                    {
                        timer = new Timer((t) => ScanPreviewBuffer(), null, 0, 1500);
                    }
                }

                //BarcodeImage.Visibility = System.Windows.Visibility.Collapsed;
                //previewRect.Visibility = System.Windows.Visibility.Visible;
        }

        private void ScanPreviewBuffer()
        {
            if (luminance == null || photoCamera == null)
                return;

            photoCamera.GetPreviewBufferY(luminance.PreviewBufferY);
            // use a dummy writeable bitmap because the luminance values are written directly to the luminance buffer
            var result = reader.Decode(dummyBitmap);
            OnResultFound(result);
        }

        public event EventHandler<ResultEventArgs> ResultFoundEvent;

        private async void OnResultFound(Result result)
        {
            //var handler = ResultFoundEvent;

            //if (handler != null)
            //{
            //    handler(this, new ResultEventArgs() { Result = result });
            //}
            
            if (result != null)
            {
                Debug.WriteLine("Barcode Type: {0}", result.BarcodeFormat);
                Debug.WriteLine("Barcode Value: {0}", result.Text);
                // if(Smartlib.Isbn.
                if (result.BarcodeFormat == BarcodeFormat.CODE_39)
                {
                    Book book = await App.CurrentApplication.BookRequests.GetBookBy(BookIdentifier.Barcode, result.Text);

                    if (book != null)
                    {
                        Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {
                            var root = Application.Current.RootVisual as Frame;
                            root.DataContext = new BookViewModel(book);
                            root.Navigate(new Uri("/MVVM/View/BookDetailsPage.xaml", UriKind.Relative));
                        });
                    }
                    else
                    {
                        Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {
                            App.CurrentApplication.MessageService.ShowSuccessMessage(Localization.search_noresult, Localization.search);

                        });
                        //return false;
                    }
                }
                else
                {
                    if (SmartLib.Isbn.IsValidIsbn13(result.Text))
                    {
                        Debug.WriteLine("{0} is ISBN13", result.Text);
                        Book book = await App.CurrentApplication.BookRequests.GetBookBy(BookIdentifier.Isbn, result.Text);

                        if (book != null)
                        {
                            Deployment.Current.Dispatcher.BeginInvoke(() =>
                            {
                                var root = Application.Current.RootVisual as Frame;
                                root.DataContext = new BookViewModel(book);
                                root.Navigate(new Uri("/MVVM/View/BookDetailsPage.xaml", UriKind.Relative));
                            });
                        }
                        else
                        {
                            string isbn10 = result.Text.Substring(3, 9);
                            Debug.WriteLine("ISBN10 9 chars: {0}", isbn10);
                            int sum = 0;
                            for (int i = 0; i < 9; i++)
                            {
                                char numberChar = isbn10[i];
                                int numberInt = Convert.ToInt16(numberChar.ToString());
                                sum += numberInt * (10 - i);
                            }

                            isbn10 += 11 - (sum % 11);

                            Debug.WriteLine("ISBN10 complete: {0}", isbn10);

                            book = await App.CurrentApplication.BookRequests.GetBookBy(BookIdentifier.Isbn, isbn10);
                            if (book != null)
                            {
                                Deployment.Current.Dispatcher.BeginInvoke(() =>
                                {
                                    var root = Application.Current.RootVisual as Frame;
                                    root.DataContext = new BookViewModel(book);
                                    root.Navigate(new Uri("/MVVM/View/BookDetailsPage.xaml", UriKind.Relative));
                                });
                            }
                            else
                            {
                                Deployment.Current.Dispatcher.BeginInvoke(() =>
                                {
                                    App.CurrentApplication.MessageService.ShowSuccessMessage(Localization.search_noresult, Localization.search);
                                });
                                //return false;
                            }
                        }
                    }
                }

            }
        }

        //private void DisplayResult(Result result)
        //{
        //    //if (result != null)
        //    //{
        //    //    BarcodeType.SelectedItem = result.BarcodeFormat;
        //    //    BarcodeContent.Text = result.Text;
        //    //}
        //    //else
        //    //{
        //    //    BarcodeType.SelectedItem = null;
        //    //    BarcodeContent.Text = "No barcode found.";
        //    //}
        //}

    }
}
