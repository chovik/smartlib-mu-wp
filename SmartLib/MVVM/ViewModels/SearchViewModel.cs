﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using SmartLib.Models;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using WP7.ScanBarCode;
using System.Windows.Threading;
using System.Threading.Tasks;
using ZXing;

namespace SmartLib.ViewModels
{
    [DataContract]
    public class SearchViewModel : BaseViewModel
    {
        private string selectedLibrary;

        public string SelectedLibrary
        {
            get { return selectedLibrary; }
            set 
            {
                if (value != selectedLibrary)
                {
                    selectedLibrary = value;
                    OnNotifyPropertyChanged("SelectedLibrary");
                }
            }
        }

        private ObservableCollection<string> libraries;
        public ObservableCollection<string> Libraries
        {
            get
            {
                if (libraries == null)
                {
                    libraries = new ObservableCollection<string>();
                }

                return libraries;
            }
        }
        

        /// <summary>
        /// Previous searched titles.
        /// </summary>
        public ObservableCollection<string> TitleHistory
        {
            get
            {
                return App.CurrentApplication.SearchedStrings[App.TITLE_KEY];
            }
        }

        /// <summary>
        /// Previous searched authors.
        /// </summary>
        public ObservableCollection<string> AuthorHistory
        {
            get
            {
                return App.CurrentApplication.SearchedStrings[App.AUTHOR_KEY];
            }
        }

        /// <summary>
        /// Previous searched isbns.
        /// </summary>
        public ObservableCollection<string> IsbnHistory
        {
            get
            {
                return App.CurrentApplication.SearchedStrings[App.ISBN_KEY];
            }
        }

        private string title;

        [DataMember]
        public string Title
        {
            get { return title; }
            set
            {
                if (value != title)
                {
                    title = value;
                    OnNotifyPropertyChanged("Title");
                }
            }
        }

        private string author;

        [DataMember]
        public string Author
        {
            get { return author; }
            set
            {
                if (value != author)
                {
                    author = value;
                    OnNotifyPropertyChanged("Author");
                }
            }
        }

        private string isbn;

        [DataMember]
        public string Isbn
        {
            get { return isbn; }
            set
            {
                if (value != isbn)
                {
                    isbn = value;
                    OnNotifyPropertyChanged("Isbn");
                }
            }
        }

        /// <summary>
        /// If command is executed, Search method will be called.
        /// </summary>
        public RelayCommand ProcessFormCommand
        {
            get;
            private set;
        }

        /// <summary>
        /// If command is executed, ScanBarcode method will be called.
        /// </summary>
        public RelayCommand ScanBarcodeCommand
        {
            get;
            private set;
        }

        public async void LoadLibraries()
        {
            Loaded = false;
            var libraries2 = await App.CurrentApplication.BookRequests.GetLibraries();

            if (libraries2 != null)
            {
                Libraries.Clear();
                Libraries.Add("--");
                foreach (var lib in libraries2)
	            {
                    Libraries.Add(lib.library);
	            }
                this.SelectedLibrary = Libraries.First();
            }

            Loaded = true;
        }


        /// <summary>
        /// Constructor.
        /// </summary>
        public SearchViewModel()
        {
            //this.validator.AddValidationFor(() => "").Must(() => 
            //    {
            //        return !string.IsNullOrWhiteSpace(this.Title) 
            //            && !string.IsNullOrWhiteSpace(this.Author) 
            //            && !string.IsNullOrWhiteSpace(this.Isbn);
            //    }).Show(Localization.enterthe + " " + Localization.title)
            //    .When(() => string.IsNullOrWhiteSpace(Isbn) && string.IsNullOrWhiteSpace(Author));

            //this.validator.AddValidationFor(() => this.Author).Must(() =>
            //{
            //    return !string.IsNullOrWhiteSpace(this.Author);
            //}).Show(Localization.enterthe + " " + Localization.author)
            //    .When(() => string.IsNullOrWhiteSpace(Isbn) && string.IsNullOrWhiteSpace(Title));

            ProcessFormCommand = new RelayCommand(() => Search());
            ScanBarcodeCommand = new RelayCommand(() => ScanBarcode());
            Loaded = true;
            LoadLibraries();
        }

        /// <summary>
        /// Validate form inputs. Returns true, if one of title, author or isbn value is filled.
        /// Shows successful/failure of validation to user.
        /// </summary>
        /// <returns>true if form inputs are valid, false otherwise</returns>
        //public bool ValidateInputs()
        //{
        //    bool isValid = true;

        //    List<string> invalidInputs = new List<string>();

        //    if (string.IsNullOrWhiteSpace(this.Isbn)
        //        && string.IsNullOrWhiteSpace(this.Title)
        //        && string.IsNullOrWhiteSpace(this.Author))
        //    {
        //        isValid = false;

        //        var msg = "One of fields must be filled.";

        //        App.CurrentApplication.MessageService.ShowWarningMessage(msg, "Search");
        //    }

        //    return isValid;
        //}

        public async void ScanBarcode()
        {
            var root = Application.Current.RootVisual as Frame;
            root.Navigate(new Uri("/MVVM/View/ScanPage.xaml", UriKind.Relative));
        }

        /// <summary>
        /// Tries to find book according filled parameters. 
        /// If ISBN is filled, parameters author and title are ignored.
        /// </summary>
        public async void Search()
        {
            //if (this.ValidateInputs())
            if (!string.IsNullOrWhiteSpace(this.Title)
                        || !string.IsNullOrWhiteSpace(this.Author)
                        || !string.IsNullOrWhiteSpace(this.Isbn))
            {
                //{
                bool isbnParam = true;
                if (!string.IsNullOrWhiteSpace(Title))
                {
                    isbnParam = false;
                    if (!this.TitleHistory.Contains(this.Title))
                        this.TitleHistory.Add(this.Title);
                }

                if (!string.IsNullOrWhiteSpace(Author))
                {
                    isbnParam = false;
                    if (!this.AuthorHistory.Contains(this.Author))
                        this.AuthorHistory.Add(this.Author);
                }

                if (!string.IsNullOrWhiteSpace(Isbn))
                {
                    isbnParam &= true;
                    if (!this.IsbnHistory.Contains(this.Author))
                        this.IsbnHistory.Add(this.Author);
                }

                Loaded = false;

                if (!isbnParam)
                {
                    var resultsViewModel = new ResultsViewModel(this.Title, this.Author, this.SelectedLibrary);

                    resultsViewModel.LoadingFinished += (s, e) =>
                        {
                            if (e.Value)
                            {
                                Loaded = true;

                                if (resultsViewModel.Books.Count > 0)
                                {
                                    var root = Application.Current.RootVisual as Frame;
                                    root.DataContext = resultsViewModel;
                                    root.Navigate(new Uri(string.Format("/MVVM/View/SearchResultsPage.xaml?text={0}", this.Title), UriKind.Relative));
                                }
                                else
                                {
                                    App.CurrentApplication.MessageService.ShowErrorMessage(Localization.search_noresult, Localization.search);
                                }
                            }
                        };

                    resultsViewModel.FetchMoreBooks();
                }
                else
                {
                    Book book = await App.CurrentApplication.BookRequests.GetBookBy(BookIdentifier.Isbn, this.Isbn);

                    if (book != null)
                    {
                        Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {
                            var root = Application.Current.RootVisual as Frame;
                            root.DataContext = new BookViewModel(book);
                            root.Navigate(new Uri("/MVVM/View/BookDetailsPage.xaml", UriKind.Relative));
                        });
                    }
                    else
                    {
                        App.CurrentApplication.MessageService.ShowErrorMessage(Localization.search_noresult, Localization.search);
                    }
                }
            }
            else
            {
                App.CurrentApplication.MessageService.ShowWarningMessage(Localization.search_validation_msg, Localization.search);
            }
       
            //}
        }
    }
}
