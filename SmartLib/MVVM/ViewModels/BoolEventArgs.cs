﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SmartLib.MVVM.ViewModels
{
    public class BoolEventArgs : EventArgs
    {
        public bool Value
        {
            get;
            set;
        }

        public BoolEventArgs(bool value)
        {
            Value = value;
        }
    }
}
