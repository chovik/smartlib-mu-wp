﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Runtime.Serialization;
using SmartLib.Models;
using System.Linq;
using System.Diagnostics;

namespace SmartLib.ViewModels
{
    [DataContract]
    public class ServerBooksViewModel : BooksViewModel
    {

        public override System.Collections.ObjectModel.ObservableCollection<BookViewModel> Books
        {
            get
            {
                if(BooksCategory == BookListCategory.News)
                {
                    return BookManager.Instance.RecentlyRatedBooks;                
                }
                else //if (BookListCategory == BookListCategory.Top)
                {
                    return BookManager.Instance.TopRatedBooks;
                }
            }
            set
            {
                base.Books = value;
            }
        }
        /// <summary>
        /// Category of loaded books. (News or Top)
        /// </summary>
        [DataMember]
        public BookListCategory BooksCategory { get; set; }

        public ServerBooksViewModel(BookListCategory category)
        {
            this.BooksCategory = category;
        }

        private RelayCommand reloadCommand;

        /// <summary>
        /// If command is executed, UpdateBooks method will be called.
        /// </summary>
        public RelayCommand ReloadCommand
        {
            get
            {
                if (reloadCommand == null) // RelayCommand is not serializable.
                    reloadCommand = new RelayCommand(() => UpdateBooks());

                return reloadCommand;
            }
            private set
            {
                if (value != reloadCommand)
                    reloadCommand = value;
            }
        }

        /// <summary>
        /// Updates books.
        /// </summary>
        public async virtual void UpdateBooks()
        {
            ConnectionErrorOccured = false;
            Loaded = false;
            //this.Books.Clear();
            Debug.WriteLine("Started book updating");
            try
            {
                //var books = await App.CurrentApplication.BookRequests.GetBooksByCategory(this.BooksCategory, 10, 0);

                if (BooksCategory == BookListCategory.News)
                {
                    await BookManager.Instance.LoadRecentlyRatedBooks();
                }
                else
                {
                    await BookManager.Instance.LoadTopRatedBooks();
                }
                Debug.WriteLine("Books updated");

                //if (books != null)
                //{
                //    foreach (Book book in books)
                //    {
                //        BookViewModel bookViewModel = new BookViewModel(book);
                //        bookViewModel.CheckCoverExistence();
                //        this.Books.Add(bookViewModel);
                //    }
                //}

            }
            catch (WebException webEx)
            {
                ConnectionErrorOccured = true;
            }
            finally
            {
                Loaded = true;
            }

        }
    }
}
