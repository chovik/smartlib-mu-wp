﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Runtime.Serialization;
using SmartLib.ViewModels;
using System.Diagnostics;

namespace SmartLib.MVVM.ViewModels
{
    public class ChangePasswordViewModel : BaseViewModel
    {
        private string oldPassword;
        /// <summary>
        /// Old password.
        /// </summary>
        [DataMember]
        public string OldPassword
        {
            get { return oldPassword; }
            set 
            {
                if (value != oldPassword)
                {
                    oldPassword = value;
                    OnNotifyPropertyChanged("OldPassword");
                }
            }
        }

        private string newPassword;

        /// <summary>
        /// New password.
        /// </summary>
        [DataMember]
        public string NewPassword
        {
            get { return newPassword; }
            set
            {
                if (value != newPassword)
                {
                    newPassword = value;
                    OnNotifyPropertyChanged("NewPassword");
                    OnNotifyPropertyChanged("NewPassword2");
                }
            }
        }

        private string newPassword2;

        /// <summary>
        /// New password again.
        /// </summary>
        [DataMember]
        public string NewPassword2
        {
            get { return newPassword2; }
            set
            {
                if (value != newPassword2)
                {
                    newPassword2 = value;
                    OnNotifyPropertyChanged("NewPassword2");
                }
            }
        }

        /// <summary>
        /// If command is executed, ChangePassword method will be called.
        /// </summary>
        public RelayCommand ProcessFormCommand
        {
            get;
            private set;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public ChangePasswordViewModel()
        {
            this.validator.AddValidationFor(() => this.OldPassword).NotEmpty().Show(Localization.enterthe + " " + Localization.oldPassword);
            this.validator.AddValidationFor(() => this.NewPassword).NotEmpty().Show(Localization.enterthe + " " + Localization.newPassword);
            //this.validator.AddValidationFor(() => this.NewPassword).NotEmpty().Show("Enter the new password(again)");
            this.validator.AddValidationFor(() => this.NewPassword2).Must(() => this.NewPassword == this.NewPassword2).Show(Localization.repeatedPasswordDiff);
            ProcessFormCommand = new RelayCommand(() => ChangePassword());
        }

        /// <summary>
        /// Logs user in.
        /// </summary>
        public async void ChangePassword()
        {
            //validate form inputs
            if (!this.ValidateAll())
                return;

            try
            {
                Loaded = false;
                //send request "save review" to server
                HttpStatusCode statusCode = await App.CurrentApplication.UserRequests.ChangePassword(App.CurrentApplication.LoggedUco, OldPassword, NewPassword);

                //process returned status code (from server response)
                switch (statusCode)
                {
                    case HttpStatusCode.OK:
                        App.CurrentApplication.MessageService.ShowSuccessMessage(Localization.password_changed, Localization.password);

                        var root = Application.Current.RootVisual as Frame;
                        root.GoBack();
                        break;
                    case HttpStatusCode.Unauthorized:
                        App.CurrentApplication.MessageService.ShowErrorMessage(Localization.need_auth, Localization.password);
                        break;
                    default:
                        Debug.WriteLine("Change password - Unxpected Status Code '{0}'.", statusCode);
                        break;
                }
            }
            catch (WebException ex)
            {
                App.CurrentApplication.MessageService.ShowErrorMessage(Localization.server_error, Localization.changePassword);
            }
            finally
            {
                Loaded = true;
            }
        }
    }
}
