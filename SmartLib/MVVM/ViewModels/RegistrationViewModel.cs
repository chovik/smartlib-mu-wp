﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Diagnostics;

namespace SmartLib.ViewModels
{
    public class RegistrationViewModel : BaseViewModel
    {

        public RelayCommand ProcessFormCommand
        {
            get;
            private set;
        }

        private string uco;

        /// <summary>
        /// Uco.
        /// </summary>
        public string Uco
        {
            get { return uco; }
            set 
            {
                if (value != uco)
                {
                    uco = value;
                    OnNotifyPropertyChanged("Uco");
                }
            }
        }

        private string firstName;

        /// <summary>
        /// First name.
        /// </summary>
        public string FirstName
        {
            get { return firstName; }
            set 
            {
                if (value != firstName)
                {
                    firstName = value;
                    OnNotifyPropertyChanged("FirstName");
                }
            }
        }

        private string lastName;

        /// <summary>
        /// Last name.
        /// </summary>
        public string LastName
        {
            get { return lastName; }
            set
            {
                if (value != lastName)
                {
                    lastName = value;
                    OnNotifyPropertyChanged("LastName");
                }
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public RegistrationViewModel()
        {
            this.validator.AddValidationFor(() => this.Uco).Must(() => !string.IsNullOrWhiteSpace(this.Uco)).Show(Localization.enterthe + " " + Localization.uco);
            this.validator.AddValidationFor(() => this.Uco)
                .Must(() =>
                {
                    int parsedUCO = -1;
                    int.TryParse(this.Uco, out parsedUCO);
                    return parsedUCO > 0;
                }).Show(Localization.invalidUco);
            this.validator.AddValidationFor(() => this.FirstName).NotEmpty().Show(Localization.enterthe + " " + Localization.firstName);
            this.validator.AddValidationFor(() => this.LastName).NotEmpty().Show(Localization.enterthe + " " + Localization.lastName);
            ProcessFormCommand = new RelayCommand(() => SendRegistration());
        }


        /// <summary>
        /// Validate form inputs. Returns true, if first name, last name and UCO are filled.
        /// Shows successful/failure of validation to user.
        /// </summary>
        /// <returns>true if form inputs are valid, false otherwise</returns>
        public bool ValidateInputs()
        {
            bool isValid = true;

            List<string> invalidInputs = new List<string>();

            if (string.IsNullOrWhiteSpace(this.FirstName))
            {
                isValid = false;
                invalidInputs.Add(Localization.firstName);
            }

            if (string.IsNullOrWhiteSpace(this.LastName))
            {
                isValid = false;
                invalidInputs.Add(Localization.lastName);
            }

            if (!isValid)
            {
                var msg = string.Format(Localization.login_error1,
                            string.Join("\n", invalidInputs));

                App.CurrentApplication.MessageService.ShowWarningMessage(msg, Localization.signUp);
            }

            return isValid;
        }


        /// <summary>
        /// Sends registration. Registration will be sent just in case that form inputs are valid.
        /// Shows successful/failure of request to user.
        /// </summary>
        public async void SendRegistration()
        {
            if (!this.ValidateAll())
                return;
            Loaded = false;
            try
            {
                HttpStatusCode statusCode = await App.CurrentApplication.UserRequests.SignUp(Uco, FirstName, LastName);

                switch (statusCode)
                {
                    case HttpStatusCode.OK:
                        App.CurrentApplication.MessageService.ShowSuccessMessage(Localization.signup_succ, Localization.signUp);
                        break;
                    case HttpStatusCode.BadRequest:
                        App.CurrentApplication.MessageService.ShowErrorMessage(Localization.signup_notexists, Localization.signUp);
                        break;
                    case HttpStatusCode.Conflict:
                        App.CurrentApplication.MessageService.ShowErrorMessage(Localization.signup_exists, Localization.signUp);
                        break;
                    default:
                        Debug.WriteLine("Unxpected Status Code '{0}'.", statusCode);
                        break;
                }
            }
            catch (WebException ex)
            {
                App.CurrentApplication.MessageService.ShowErrorMessage(Localization.server_error, Localization.signUp);
            }
            finally
            {
                Loaded = true;
            }
        }
        
        
    }
}
