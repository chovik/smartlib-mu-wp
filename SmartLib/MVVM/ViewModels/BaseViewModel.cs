﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using SmartLib.Helpers;
using System.Runtime.Serialization;
using Infrastructure.Validation;
using SmartLib.MVVM.ViewModels;

namespace SmartLib.ViewModels
{
    /// <summary>
    /// Base ViewModel. Contains common methods, properties of all ViewModels.
    /// </summary>
    [DataContract]
    [KnownType(typeof(BooksViewModel))]
    [KnownType(typeof(BookViewModel))]
    [KnownType(typeof(ReviewViewModel))]
    public abstract class BaseViewModel : ValidationViewModel, INotifyPropertyChanged
    {

        protected bool connectionErrorOccured = false;
        
        /// <summary>
        /// Gets or sets a value indicating whether connection error occured.
        /// </summary>
        public virtual bool ConnectionErrorOccured
        {
            get
            {
                return connectionErrorOccured;
            }
            set
            {
                if(value != connectionErrorOccured)
                {
                    connectionErrorOccured = value;
                    OnNotifyPropertyChanged("ConnectionErrorOccured");
                    OnConnectionErrorOccured();
                    //if (value)
                    //{
                    //    App.CurrentApplication.MessageService.ShowErrorMessage("The server is not responding , please check your connection and try again.",
                    //    ConnectionErrorTitle);
                    //}
                }
            }
        }

        public event EventHandler ConnectionErrorOccuredHandler;

        public void OnConnectionErrorOccured()
        {
            var handler = ConnectionErrorOccuredHandler;

            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        protected string ConnectionErrorTitle = "Connection Error";


        protected int loadedMonitor = 0;
        private bool loaded = true;

        /// <summary>
        /// Gets or sets a value indicating whether the list of books is loaded or not.
        /// True if books are loaded, false during loading.
        /// </summary>
        public bool Loaded
        {
            get
            {
                return loaded;
            }
            set
            {
                if (loaded != value)
                {
                    //if (value)
                    //{
                    //    if (loadedMonitor == 1)
                    //    {
                    //        loadedMonitor--;
                    //    }
                    //    else if (loadedMonitor > 1)
                    //    {
                    //        return;
                    //    }
                    //    else
                    //    {
                    //        loadedMonitor = 0;
                    //    }
                    //}
                    this.loaded = value;
                    OnNotifyPropertyChanged("Loaded");
                }

                //if (value)
                //{
                    OnLoadingFinished(loaded);
                //}
            }
        }

        public event EventHandler<BoolEventArgs> LoadingFinished;

        /// <summary>
        /// Vola pri chybe robota. Zasignalizuje chybu prostrednictvom event.
        /// </summary>
        /// <param name="e">obsahuje informacie o chybe.</param>
        protected virtual void OnLoadingFinished(bool loaded)
        {
            var loadedEvent = LoadingFinished;
            if (loadedEvent != null)
                loadedEvent(this, new BoolEventArgs(loaded));
        }
    }
}
