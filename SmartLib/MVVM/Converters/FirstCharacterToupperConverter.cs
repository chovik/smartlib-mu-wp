﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Globalization;
using System.Diagnostics;

namespace SmartLib.Helpers
{
    public class FirstCharacterToupperConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {

            var str = value as string;

            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
                // Return char and concat substring.
            return char.ToUpper(str[0]) + str.Substring(1);
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return string.Empty;
        }
    }
}
