﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Data;

namespace SmartLib.MVVM.Converters
{
    public class ErrorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var errors = value as ReadOnlyObservableCollection<ValidationError>;
            string errorText = "error";
            if(errors != null)
            {
                foreach(var error in errors)
                {
                    errorText += error.ErrorContent;
                }
            }
            return errorText;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
