﻿using SLMultiBinding;
using SmartLib.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Util.Converters
{
    public class LineSplitConverter : IMultiValueConverter
    {

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values == null || values.Count() == 0)
            {
                return values;
            }

            string text = values[0] as string;
            TextBlock textbox = values[1] as TextBlock;
            string formatedText = null;

            if (text != null
                && textbox != null)
            {
                //textbox.Text = text;
                TextBlock temp = new TextBlock();
                temp.FontFamily = textbox.FontFamily;
                temp.FontSize = textbox.FontSize;
                temp.FontWeight = textbox.FontWeight;
                temp.FontStyle = textbox.FontStyle;
                temp.FontStretch = textbox.FontStretch;
                formatedText = "";
                foreach (var character in text)
                {
                    temp.Text += character;
                    if (temp.ActualWidth >= 200)
                    {
                        formatedText += temp.Text + Environment.NewLine;
                        temp.Text = "";
                    }
                }

                formatedText += temp.Text;
            }
            //string[] lines = Regex.Split(tbIn.Text, @"(?<=\r\n)(?!$)");
            return formatedText;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
