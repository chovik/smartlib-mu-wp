﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace SmartLib.MVVM.Converters
{
    public class LendTypeLocalizationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string lendType = value as string;

            if (lendType != null)
            {
                if (lendType == "abs")
                {
                    return Localization.offSite;
                }
                else if (lendType == "prs")
                {
                    return Localization.onSite;
                }
            }
            return "-";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
