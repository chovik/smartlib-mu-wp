﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using SmartLib.ViewModels;
using SmartLib.Models;
using System.Windows.Data;
using WPExtensions;
using SmartLib.Helpers;
using Microsoft.Phone.Shell;

namespace SmartLib.View
{
    public partial class BookDetailsPage : PhoneApplicationPage
    {
        private readonly string transientKey = "ViewModel";

        private bool isNewPageInstance = false;

        // private MainViewModel viewModel;

        public BookViewModel ViewModel
        {
            get
            {
                return DataContext as BookViewModel;
            }
        }

        public BookDetailsPage()
        {
            isNewPageInstance = true;
            InitializeComponent();
            Loaded += BookDetailsPage_Loaded;
            
            //Loaded += BookDetailsPage_Loaded;
        }

        AdvancedApplicationBarIconButton previewButton;
        AdvancedApplicationBarIconButton addFavButton;
        AdvancedApplicationBarIconButton shareButton;
        AdvancedApplicationBarIconButton refreshButton;

        /// <summary>
        /// Finds a Child of a given item in the visual tree. 
        /// </summary>
        /// <param name="parent">A direct parent of the queried item.</param>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="childName">x:Name or Name of child. </param>
        /// <returns>The first parent item that matches the submitted type parameter. 
        /// If not matching item can be found, 
        /// a null parent is being returned.</returns>
        public static T FindChild<T>(DependencyObject parent, string childName)
           where T : DependencyObject
        {
            // Confirm parent and childName are valid. 
            if (parent == null) return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree
                    foundChild = FindChild<T>(child, childName);

                    // If the child is found, break so we do not overwrite the found child. 
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found.
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }

        TextBlock errorTextBlock = null;

        void BookDetailsPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (appbar == null)
            {
                appbar = new AdvancedApplicationBar();
            }
            appbar.IsMenuEnabled = false;

            previewButton = new AdvancedApplicationBarIconButton() { Text = Localization.preview, IconUri = new Uri("/Images/appbar.preview.rest.png", UriKind.Relative) };
            previewButton.Click += previewButton_Click;
            appbar.Items.Add(previewButton);

            addFavButton = new AdvancedApplicationBarIconButton() { Text = Localization.addToFavourites, IconUri = new Uri("/Images/appbar.favs.addto.rest.png", UriKind.Relative) };
            addFavButton.Click += addFavButton_Click;
            appbar.Items.Add(addFavButton);

            shareButton = new AdvancedApplicationBarIconButton() { Text = Localization.share, IconUri = new Uri("/Images/OpenShareIcon.png", UriKind.Relative) };
            shareButton.Click += shareButton_Click;
            appbar.Items.Add(shareButton);

            refreshButton = new AdvancedApplicationBarIconButton() { Text = Localization.reload, IconUri = new Uri("/Images/appbar.refresh.rest.png", UriKind.Relative) };
            refreshButton.Click += refreshButton_Click;
            appbar.Items.Add(refreshButton);

            //appbar.ButtonItems.Add(previewButton);
            //appbar.ButtonItems.Add(addFavButton);
            //appbar.ButtonItems.Add(refreshButton);
            appbar.ReCreateAppBar();

            errorTextBlock = FindChild<TextBlock>(pivotControl, "errorTextBlock");

            if (errorTextBlock != null)
            {
                errorTextBlock.Visibility = System.Windows.Visibility.Collapsed;
            }

            PageLoaded = true;

            //ViewModel_IsBookmarkedChanged(null, null);
            //this.ApplicationBar = appbar;
        }

        void shareButton_Click(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.ShareBook();
            }
        }

          

        void refreshButton_Click(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.ReloadData();
            }
        }

        void addFavButton_Click(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.Bookmark();
            }
        }

        void previewButton_Click(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.OpenPreview();
            }
        }

        //void BookDetailsPage_Loaded(object sender, RoutedEventArgs e)
        //{
            
        //    //Binding myBinding = new Binding("IsBookmarked");
        //    //myBinding.Converter = new BooleanToUriConverter("/Images/appbar.favs.remove.rest.png", "/Images/appbar.favs.addto.rest.png");
        //    //myBinding.TargetNullValue = "/Images/appbar.favs.remove.rest.png";
        //    //myBinding.Source = ViewModel;
        //    //appbar_bookmark.SetBinding(AdvancedApplicationBarIconButton.IconUriProperty, myBinding);
        //}

        void ViewModel_IsBookmarkedChanged(object sender, EventArgs e)
        {
            if (ViewModel.IsBookmarked)
            {
                addFavButton.IconUri = new Uri("/Images/appbar.favs.remove.rest.png", UriKind.Relative);
                addFavButton.Text = Localization.removeFromFavourites;
            }
            else
            {
                addFavButton.IconUri = new Uri("/Images/appbar.favs.addto.rest.png", UriKind.Relative);
                addFavButton.Text = Localization.addToFavourites;
            }
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            if (e.NavigationMode != System.Windows.Navigation.NavigationMode.Back)
            {
                if (State.ContainsKey(transientKey))
                {
                    State[transientKey] = ViewModel;
                }
                else
                {
                    State.Add(transientKey, ViewModel);
                }
            }

        }

        private bool viewModelLoaded = false;
        public bool ViewModelLoaded
        {
            get
            {
                return viewModelLoaded;
            }
            private set
            {
                if(value != viewModelLoaded)
                {
                    viewModelLoaded = value;

                    if (ViewModelLoaded && PageLoaded)
                    {
                        ViewModel_IsBookmarkedChanged(null,null);
                    }
                }
            }
        }

        private bool pageLoaded = false;
        public bool PageLoaded
        {
            get
            {
                return pageLoaded;
            }
            private set
            {
                if (value != pageLoaded)
                {
                    pageLoaded = value;

                    if (ViewModelLoaded && PageLoaded)
                    {
                        ViewModel_IsBookmarkedChanged(null, null);
                    }
                }
            }
        }

        protected async override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            string sysno = null;
            NavigationContext.QueryString.TryGetValue("sysno", out sysno);

            if (isNewPageInstance)
            {
                if (ViewModel == null)
                {
                    object viewModel = null;

                    if (State.TryGetValue(transientKey, out viewModel))
                    {
                        DataContext = viewModel;
                    }
                    else
                    {
                        if (sysno != null)
                        {
                            DataContext = await BookManager.Instance.GetDetailedBook(sysno);
                        }
                        else
                        {
                            DataContext = new BookViewModel(new Book());
                        }
                    }

                    ViewModel.ConnectionErrorOccuredHandler += ViewModel_ConnectionErrorOccuredHandler;
                }
                else
                {
                    ViewModel.UpdateBookDetails();
                }

                ViewModel.IsBookmarkedChanged += ViewModel_IsBookmarkedChanged;
            }

            ViewModel.UpdateRating();
            ViewModel.UpdateReviews();
            ViewModel.UpdateCopies();
            //ViewModel.CheckCoverExistence();

            if (e.NavigationMode == System.Windows.Navigation.NavigationMode.New)
            {
                ViewModel.AddToHistory();
                //DataContext = ViewModel;
            }

            //ViewModel_IsBookmarkedChanged(null, null);
            ViewModelLoaded = true;
            isNewPageInstance = false;
        }

        void ViewModel_ConnectionErrorOccuredHandler(object sender, EventArgs e)
        {
            if (errorTextBlock != null && ViewModel != null)
            {
                if(ViewModel.ConnectionErrorOccured)
                {
                    errorTextBlock.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    errorTextBlock.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.CheckCurrentAvailability();
            }
        }
    }
}