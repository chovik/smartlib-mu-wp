﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using SmartLib;
using SmartLib.ViewModels;
using System.Windows.Data;
using SmartLib.MVVM.ViewModels;
using WPExtensions;
using System.Diagnostics;
using SmartLib.Helpers;

namespace SmartLib.View
{
    public partial class MainPage : PhoneApplicationPage
    {
        private readonly string transientKey = "ViewModel";

        private bool isNewPageInstance = false;

        // private MainViewModel viewModel;

        public MainViewModel ViewModel
        {
            get
            {
                return DataContext as MainViewModel;
            }
        }

        AdvancedApplicationBarMenuItem logInMenuItem = new AdvancedApplicationBarMenuItem() { Text = Localization.logIn };
        AdvancedApplicationBarMenuItem signUpMenuItem = new AdvancedApplicationBarMenuItem() { Text = Localization.signUp };
        AdvancedApplicationBarMenuItem changePasswordMenuItem = new AdvancedApplicationBarMenuItem() { Text = Localization.changePassword };
        AdvancedApplicationBarMenuItem logOutMenuItem = new AdvancedApplicationBarMenuItem() { Text = Localization.logOut };
        // Constructor
        public MainPage()
        {
            isNewPageInstance = true;
            InitializeComponent();
            Loaded += MainPage_Loaded;


            

            
            searchIconButton.Click += searchMenuItem_Click;
            appbar.Items.Add(searchIconButton);

            scanIconButton.Click += scanIconButton_Click;
            appbar.Items.Add(scanIconButton);

            selectionModeAppButton.Click += selectionModeAppButton_Click;
            appbar.Items.Add(selectionModeAppButton);

            //refreshButton.Click += refreshButton_Click;
            appbar.Items.Add(refreshButton);

            
            logInMenuItem.Click += logInMenuItem_Click;
            
            appbar.Items.Add(logInMenuItem);

            
            
            signUpMenuItem.Click += signUpMenuItem_Click;
            appbar.Items.Add(signUpMenuItem);

            
            
            changePasswordMenuItem.Click += changePasswordMenuItem_Click;
            appbar.Items.Add(changePasswordMenuItem);

            
            
            logOutMenuItem.Click += logOutMenuItem_Click;
            appbar.Items.Add(logOutMenuItem);

            

            var settingsMenuItem = new AdvancedApplicationBarMenuItem() { Text = Localization.settings };
            settingsMenuItem.Click += settingsMenuItem_Click;
            appbar.Items.Add(settingsMenuItem);

            appbar_removeButton.Click += appbar_removeButton_Click;
            appbar.Items.Add(appbar_removeButton);

            //appbar.ButtonItems.Add(previewButton);
            //appbar.ButtonItems.Add(addFavButton);
            //appbar.ButtonItems.Add(refreshButton);

            App.CurrentApplication.LoginChanged += (s, e) => ViewModel_IsLoggedChanged(s, e);
            
        }

        void appbar_removeButton_Click(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.RemoveSelectedFavourites();
            }
        }

        void selectionModeAppButton_Click(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.SwitchSelectionMode();
            }
        }

        AdvancedApplicationBarIconButton selectionModeAppButton = new AdvancedApplicationBarIconButton()
        {
            Text = Localization.edit,
            IconUri = new Uri("/Images/appbar.edit.rest.png", UriKind.Relative),
        };

        AdvancedApplicationBarIconButton appbar_removeButton = new AdvancedApplicationBarIconButton()
        {
            Text = Localization.remove,
            IconUri = new Uri("/Images/appbar.delete.rest.png", UriKind.Relative),
            Visibility = Visibility.Collapsed
        };

        AdvancedApplicationBarIconButton searchIconButton = new AdvancedApplicationBarIconButton()
        {
            Text = Localization.search,
            IconUri = new Uri("/Images/appbar.feature.search.rest.png", UriKind.Relative),
        };
        AdvancedApplicationBarIconButton scanIconButton = new AdvancedApplicationBarIconButton()
        {
            Text = Localization.scanBarcode,
            IconUri = new Uri("/Images/appbar.feature.camera.rest.png", UriKind.Relative)
        };
        AdvancedApplicationBarIconButton refreshButton = new AdvancedApplicationBarIconButton()
        {
            Text = Localization.reload,
            IconUri = new Uri("/Images/appbar.refresh.rest.png", UriKind.Relative)
        };

        TextBlock errorTextBlock;

        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("Main page laoded");

            //Binding loginBinding = new Binding("IsLoggedIn");
            //loginBinding.Source = this.DataContext;
            //loginBinding.Converter = new BooleanToVisibilityConverter();
            //loginBinding.ConverterParameter = "Collapsed";
            //loginBinding.Mode = BindingMode.TwoWay;

            //Binding loginBinding2 = new Binding("IsLoggedIn");
            //loginBinding2.Source = this.DataContext;
            //loginBinding2.Converter = new BooleanToVisibilityConverter();
            //loginBinding.ConverterParameter = "Visible";
            //loginBinding2.Mode = BindingMode.TwoWay;

            //logInMenuItem.SetBinding(AdvancedApplicationBarMenuItem.VisibilityProperty, loginBinding);
            //signUpMenuItem.SetBinding(AdvancedApplicationBarMenuItem.VisibilityProperty, loginBinding);
            //changePasswordMenuItem.SetBinding(AdvancedApplicationBarMenuItem.VisibilityProperty, loginBinding2);
            //logOutMenuItem.SetBinding(AdvancedApplicationBarMenuItem.VisibilityProperty, loginBinding2);
            ViewModel_IsLoggedChanged(null, null);
            appbar.ReCreateAppBar();

            errorTextBlock = BookDetailsPage.FindChild<TextBlock>(pivotControl, "errorTextBlock");

            if (errorTextBlock != null)
            {
                errorTextBlock.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        void ViewModel_ConnectionErrorOccuredHandler(object sender, EventArgs e)
        {
            if (errorTextBlock != null && ViewModel != null)
            {
                if (ViewModel.ConnectionErrorOccured)
                {
                    errorTextBlock.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    errorTextBlock.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }
        

        void settingsMenuItem_Click(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.GoToSettingsPage();
            }
        }

        void logOutMenuItem_Click(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.LogOut();
            }
        }

        void changePasswordMenuItem_Click(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.GoToChangePasswordPage();
            }
        }

        void signUpMenuItem_Click(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.GoToRegistrationPage();
            }
        }

        void logInMenuItem_Click(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.GoToLoginPage();
            }
        }

        void refreshButton_Click(object sender, EventArgs e)
        {
            //if (ViewModel != null)
            //{
            //    ViewModel.Ref();
            //}
        }

        void scanIconButton_Click(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.GoToScanPage();
            }
        }

        void searchMenuItem_Click(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.GoToSearchPage();
            }
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            if (e.NavigationMode != System.Windows.Navigation.NavigationMode.Back)
            {
                if (State.ContainsKey(transientKey))
                {
                    State[transientKey] = ViewModel;
                }
                else
                {
                    State.Add(transientKey, ViewModel);
                }
            }

        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            Debug.WriteLine("Main Page navigated to - START");
            base.OnNavigatedTo(e);

            if (e.NavigationMode == System.Windows.Navigation.NavigationMode.New)
            {
                while (NavigationService.BackStack.Count() > 0)
                {
                    NavigationService.RemoveBackEntry();
                }
            }

            if (isNewPageInstance)
            {
                if (ViewModel == null)
                {
                    object viewModel = null;

                    if (State.TryGetValue(transientKey, out viewModel))
                    {
                        DataContext = viewModel;
                    }
                    else
                    {
                        DataContext = new MainViewModel();
                        ViewModel.LoadDataFromServer();

                        ViewModel.HistoryViewModel.UpdateRatings();
                        ViewModel.FavouriteBooksViewModel.UpdateRatings();
                        ViewModel.FavouriteBooksViewModel.SelectionModeChanged += (s, ev) =>
                            {
                                if (ViewModel.FavouriteBooksViewModel.IsSelectionMode)
                                {

                                }
                            };
                    }
                    ViewModel.ConnectionErrorOccuredHandler +=ViewModel_ConnectionErrorOccuredHandler;
                    ViewModel.SelectionModeChanged += ViewModel_SelectionModeChanged;
                    ViewModel.IsLoggedChanged += ViewModel_IsLoggedChanged;
                }
            }

            if (BooksViewModel.LastSeenBookSysno != null)
            {
                ViewModel.UpdateRatings(BooksViewModel.LastSeenBookSysno);
            }

            isNewPageInstance = false;
            Debug.WriteLine("Main Page navigated to - END");
        }

        void ViewModel_IsLoggedChanged(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                logInMenuItem.Visibility = ViewModel.IsLoggedIn ? Visibility.Collapsed : Visibility.Visible;
                signUpMenuItem.Visibility = ViewModel.IsLoggedIn ? Visibility.Collapsed : Visibility.Visible;

                changePasswordMenuItem.Visibility = !ViewModel.IsLoggedIn ? Visibility.Collapsed : Visibility.Visible;
                logOutMenuItem.Visibility = !ViewModel.IsLoggedIn ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        void ViewModel_SelectionModeChanged(object sender, EventArgs e)
        {
            //if (pivotControl.SelectedIndex == 3)
            //{
                if (ViewModel.IsSelectionMode)
                {
                    selectionModeAppButton.IconUri = new Uri("/Images/appbar.back.rest.png", UriKind.Relative);
                    selectionModeAppButton.Text = Localization.back;
                    searchIconButton.Visibility = System.Windows.Visibility.Collapsed;
                    scanIconButton.Visibility = System.Windows.Visibility.Collapsed;
                    refreshButton.Visibility = System.Windows.Visibility.Collapsed;
                    //appbar.Items.Add(appbar_removeButton);
                    appbar_removeButton.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    selectionModeAppButton.IconUri = new Uri("/Images/appbar.edit.rest.png", UriKind.Relative);
                    selectionModeAppButton.Text = Localization.edit;
                    appbar_removeButton.Visibility = System.Windows.Visibility.Collapsed;
                    //appbar.Items.Remove(appbar_removeButton);
                    searchIconButton.Visibility = System.Windows.Visibility.Visible;
                    scanIconButton.Visibility = System.Windows.Visibility.Visible;
                    refreshButton.Visibility = System.Windows.Visibility.Visible;
                    
                    
                }
            //}
        }

        EventHandler<BoolEventArgs> loadedHandler;

        public EventHandler<BoolEventArgs> LoadedHandler
        {
            get
            {
                if (loadedHandler == null)
                {
                    loadedHandler = (s, e) =>
                        {
                            ViewModel.Loaded = e.Value;
                            //Debug.WriteLine("Loaded, " + e.Value);
                        };
                }
                return loadedHandler;
            }
        }

        private void Pivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int pivotIndex = ((Pivot)sender).SelectedIndex;
            Binding visibilityBinding = null;
            switch (pivotIndex)
            {
                case 0:
                    visibilityBinding = new Binding("NewBooksViewModel.ConnectionErrorOccured");
                    refreshButton.Command = ViewModel.NewBooksViewModel.ReloadCommand;
                    ViewModel.Loaded = ViewModel.NewBooksViewModel.Loaded;
                    ViewModel.TopBooksViewModel.LoadingFinished -= LoadedHandler;
                    ViewModel.HistoryViewModel.LoadingFinished -= LoadedHandler;
                    ViewModel.FavouriteBooksViewModel.LoadingFinished -= LoadedHandler;
                    ViewModel.NewBooksViewModel.LoadingFinished += LoadedHandler;
                    
                    //if(appBar.MenuItems.Contains(Resources["bacToNormalModeButton"]))
                    //{
                    //    appBar.MenuItems.Remove(Resources["bacToNormalModeButton"]);
                    //}
                    //if(!appBar.MenuItems.Contains(Resources["settingsButton"]))
                    //{
                    //    appBar.MenuItems.Add(Resources["settingsButton"]);
                    //}
                    break;
                case 1:
                    visibilityBinding = new Binding("TopBooksViewModel.ConnectionErrorOccured");
                    refreshButton.Command = ViewModel.TopBooksViewModel.ReloadCommand;
                    ViewModel.Loaded = ViewModel.TopBooksViewModel.Loaded;
                    ViewModel.TopBooksViewModel.LoadingFinished += LoadedHandler;
                    ViewModel.HistoryViewModel.LoadingFinished -= LoadedHandler;
                    ViewModel.FavouriteBooksViewModel.LoadingFinished -= LoadedHandler;
                    ViewModel.NewBooksViewModel.LoadingFinished -= LoadedHandler;
                    //if(appBar.MenuItems.Contains(Resources["bacToNormalModeButton"]))
                    //{
                    //    appBar.MenuItems.Remove(Resources["bacToNormalModeButton"]);
                    //}
                    //if(!appBar.MenuItems.Contains(Resources["settingsButton"]))
                    //{
                    //    appBar.MenuItems.Add(Resources["settingsButton"]);
                    //}
                    break;
                case 2:
                    visibilityBinding = new Binding("HistoryViewModel.ConnectionErrorOccured");
                    refreshButton.Command = ViewModel.HistoryViewModel.UpdateRatingsCommand;
                    ViewModel.Loaded = ViewModel.HistoryViewModel.Loaded;
                    ViewModel.TopBooksViewModel.LoadingFinished -= LoadedHandler;
                    ViewModel.HistoryViewModel.LoadingFinished += LoadedHandler;
                    ViewModel.FavouriteBooksViewModel.LoadingFinished -= LoadedHandler;
                    ViewModel.NewBooksViewModel.LoadingFinished -= LoadedHandler;
                    //if(appBar.MenuItems.Contains(Resources["bacToNormalModeButton"]))
                    //{
                    //    appBar.MenuItems.Remove(Resources["bacToNormalModeButton"]);
                    //}
                    //if(!appBar.MenuItems.Contains(Resources["settingsButton"]))
                    //{
                    //    appBar.MenuItems.Add(Resources["settingsButton"]);
                    //}
                    break;
                case 3:
                    visibilityBinding = new Binding("FavouriteBooksViewModel.ConnectionErrorOccured");
                    refreshButton.Command = ViewModel.FavouriteBooksViewModel.UpdateRatingsCommand;
                    ViewModel.Loaded = ViewModel.HistoryViewModel.Loaded;
                    ViewModel.TopBooksViewModel.LoadingFinished -= LoadedHandler;
                    ViewModel.HistoryViewModel.LoadingFinished -= LoadedHandler;
                    ViewModel.FavouriteBooksViewModel.LoadingFinished += LoadedHandler;
                    ViewModel.NewBooksViewModel.LoadingFinished -= LoadedHandler;
                    //if(!appBar.MenuItems.Contains(Resources["bacToNormalModeButton"]))
                    //{
                    //    appBar.MenuItems.Add(Resources["bacToNormalModeButton"]);
                    //}
                    //if(appBar.MenuItems.Contains(Resources["settingsButton"]))
                    //{
                    //    appBar.MenuItems.Remove(Resources["settingsButton"]);
                    //}
                    break;
            }

            if (selectionModeAppButton != null)
            {
                if (pivotIndex == 3)
                {
                    //settingsButton.Visibility = System.Windows.Visibility.Collapsed;
                    selectionModeAppButton.Visibility = System.Windows.Visibility.Visible;                    
                }
                else
                {
                    selectionModeAppButton.Visibility = System.Windows.Visibility.Collapsed;
                    if (ViewModel != null
                        && ViewModel.IsSelectionMode)
                    {
                        ViewModel.SwitchSelectionMode();
                    }
                    //settingsButton.Visibility = System.Windows.Visibility.Visible;
                }
            }
        }
    }
}