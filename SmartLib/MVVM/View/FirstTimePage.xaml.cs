﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using SmartLib.ViewModels;

namespace SmartLib.RequestManagers
{
    public partial class FirstTimePage : PhoneApplicationPage
    {
        public FirstTimePage()
        {
            isNewPageInstance = true;
            InitializeComponent();
            this.BindingValidationError += LoginPage_BindingValidationError;
            Loaded += FirstTimePage_Loaded;
        }

        ApplicationBar appbar;

        void FirstTimePage_Loaded(object sender, RoutedEventArgs e)
        {
            appbar = new ApplicationBar();

            var loginBarButton = new ApplicationBarMenuItem() { Text = Localization.logIn };
            loginBarButton.Click += loginBarButton_Click;

            var skipAppBar = new ApplicationBarMenuItem() { Text = Localization.skip };
            skipAppBar.Click += skipAppBar_Click;

            appbar.MenuItems.Add(loginBarButton);
            appbar.MenuItems.Add(skipAppBar);

            this.ApplicationBar = appbar;
        }

        private readonly string transientKey = "ViewModel";

        private bool isNewPageInstance = false;

        // private MainViewModel viewModel;

        public RegistrationViewModel ViewModel
        {
            get
            {
                return DataContext as RegistrationViewModel;
            }
        }

        private void LoginPage_BindingValidationError(object sender, ValidationErrorEventArgs e)
        {
            var state = e.Action == ValidationErrorEventAction.Added ? "Invalid" : "Valid";

            VisualStateManager.GoToState((Control)e.OriginalSource, state, false);
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            if (e.NavigationMode != System.Windows.Navigation.NavigationMode.Back)
            {
                if (State.ContainsKey(transientKey))
                {
                    State[transientKey] = ViewModel;
                }
                else
                {
                    State.Add(transientKey, ViewModel);
                }
            }

        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (isNewPageInstance)
            {
                if (ViewModel == null)
                {
                    object viewModel = null;

                    if (State.TryGetValue(transientKey, out viewModel))
                    {
                        DataContext = viewModel;
                    }
                    else
                    {
                        DataContext = new RegistrationViewModel();
                    }
                }
            }

            isNewPageInstance = false;
        }

        void loginBarButton_Click(object sender, EventArgs e)
        {
            Uri nUri = new Uri("/MVVM/View/LoginPage.xaml?nextPageURI=/MVVM/View/MainPage.xaml", UriKind.Relative);
            NavigationService.Navigate(nUri);
        }

        void skipAppBar_Click(object sender, EventArgs e)
        {
            Uri nUri = new Uri("/MVVM/View/MainPage.xaml", UriKind.Relative);
            NavigationService.Navigate(nUri);
        }
    }
}