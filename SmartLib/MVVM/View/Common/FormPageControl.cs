﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SmartLib.View.Common
{
    public class FormPageControl : ItemsControl
    {


        public string PageTitle
        {
            get { return (string)GetValue(PageTitleProperty); }
            set { SetValue(PageTitleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PageTitle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PageTitleProperty =
            DependencyProperty.Register("PageTitle", typeof(string), typeof(FormPageControl), new PropertyMetadata("default"));



        public string ButtonTitle
        {
            get { return (string)GetValue(ButtonTitleProperty); }
            set { SetValue(ButtonTitleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ButtonTitle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ButtonTitleProperty =
            DependencyProperty.Register("ButtonTitle", typeof(string), typeof(FormPageControl), new PropertyMetadata(null));

        //private static void OnButtonTitleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        //{
        //    var formPageControl = d as FormPageControl;

        //    if(formPageControl == null)
        //        return;

        //    if(formPageControl.ProcessButton == null)
        //        return;

        //    formPageControl.ProcessButton.Visibility = e.NewValue == null ? Visibility.Collapsed : Visibility.Visible;


        //}

        //public Button ProcessButton 
        //{ 
        //    get; 
        //    private set; 
        //}
      
        //public override void  OnApplyTemplate()
        //{
        //     base.OnApplyTemplate();
        //}

        public FormPageControl()
        {
            //DefaultStyleKey = typeof(FormPageControl);
        }

        
    }
}
