﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using SmartLib.Models;
using SmartLib.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using WP7.ScanBarCode;
using ZXing;

namespace SmartLib.View
{
    public partial class SearchBooksPage : PhoneApplicationPage
    {
        private readonly string transientKey = "ViewModel";

        private bool isNewPageInstance = false;

       // private MainViewModel viewModel;

        public SearchViewModel ViewModel
        {
            get 
            {
                return DataContext as SearchViewModel;
            }
        }
        

        // Constructor
        public SearchBooksPage()
        {
            isNewPageInstance = true;
            InitializeComponent();

            // Set the data context of the listbox control to the sample data
            this.Loaded += new RoutedEventHandler(MainPage_Loaded);
            this.BindingValidationError += LoginPage_BindingValidationError;
        }

        private void LoginPage_BindingValidationError(object sender, ValidationErrorEventArgs e)
        {
            var state = e.Action == ValidationErrorEventAction.Added ? "Invalid" : "Valid";

            VisualStateManager.GoToState((Control)e.OriginalSource, state, false);
        }

        ApplicationBar appbar;

        // Load data for the ViewModel Items
        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            appbar = new ApplicationBar();
            appbar.IsMenuEnabled = false;

            var scanButton = new ApplicationBarIconButton(new Uri("/Images/appbar.feature.camera.rest.png", UriKind.Relative)) { Text = Localization.scanBarcode };
            scanButton.Click += scanButton_Click;


            appbar.Buttons.Add(scanButton);

            this.ApplicationBar = appbar;
        }

        void scanButton_Click(object sender, EventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.ScanBarcode();
            }
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            if (e.NavigationMode != System.Windows.Navigation.NavigationMode.Back)
            {
                if (State.ContainsKey(transientKey))
                {
                    State[transientKey] = ViewModel;
                }
                else
                {
                    State.Add(transientKey, ViewModel);
                }
            }

        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (isNewPageInstance)
            {
                if (ViewModel == null)
                {
                    object viewModel = null;

                    if(State.TryGetValue(transientKey, out viewModel))
                    {
                        DataContext = viewModel;
                    }
                    else
                    {
                        DataContext = new SearchViewModel();
                    }
                }
            }

            isNewPageInstance = false;
        }

        private void ApplicationBarIconButton_Click_1(object sender, EventArgs e)
        {
            GoToScanPage();
        }

        public void GoToScanPage()
        {
            var root = Application.Current.RootVisual as Frame;
            root.Navigate(new Uri("/MVVM/View/ScanPage.xaml", UriKind.Relative));
        }
    }
}