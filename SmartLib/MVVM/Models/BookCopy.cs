﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Runtime.Serialization;

namespace SmartLib.MVVM.Models
{
    [DataContract]
    public class BookCopy
    {
        [DataMember(Name = "signature")]
        public string signature { get; set; }

        [DataMember(Name = "barcode")]
        public string BarCode { get; set; }

        [DataMember(Name = "last_check")]
        public string LastCheck { get; set; }

        [DataMember(Name = "status")]
        public bool Status { get; set; }

        [DataMember(Name = "col")]
        public string Col { get; set; }
        
        [DataMember(Name = "panel")]
        public string Panel { get; set; }

        [DataMember(Name = "library")]
        public string library 
        { 
            get; 
            set; 
        }

        [DataMember(Name = "type")]
        public string type { get; set; }

        public DateTime? Return { get; set; }

    }
}
