﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace SmartLib.MVVM.Models
{
    [DataContract]
    public class Availability
    {
        [DataMember(Name = "status")]
        public bool Status { get; set; }

        [DataMember(Name = "return")]
        public DateTime Return { get; set; }

        [DataMember(Name = "barcode")]
        public string Barcode { get; set; }
    }
}
