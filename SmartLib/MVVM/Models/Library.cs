﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace SmartLib.MVVM.Models
{
    [DataContract]
    public class Library
    {
        [DataMember(Name = "library")]
        public string library { get; set; }
    }
}
