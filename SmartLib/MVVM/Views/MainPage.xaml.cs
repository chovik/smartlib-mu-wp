﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using SmartLib.ViewModels;
using Microsoft.Phone.Shell;
using Phone7.Fx.Controls;
using System.Windows.Data;
using SmartLib.Helpers;
using System.Diagnostics;
using WPExtensions;

namespace SmartLib
{
    public partial class MainPage : PhoneApplicationPage
    {

        private readonly string transientKey = "ViewModel";

        private bool isNewPageInstance = false;

       // private MainViewModel viewModel;

        public MainViewModel ViewModel
        {
            get 
            {
                return DataContext as MainViewModel;
            }
        }
        

        // Constructor
        public MainPage()
        {
            var x = "asas";
            Debug.WriteLine("mainpage constructor (1)");
            isNewPageInstance = true;
            Debug.WriteLine("mainpage constructor - initializing components started");
            InitializeComponent();
            Debug.WriteLine("mainpage constructor - initializing components ended");
            App.CurrentApplication.LoginChanged += (s, e) =>
                {
                    if (e != null)
                    {
                        if (e.IsLoggedIn)
                        {
                            changePasswordMenuItem.Visibility = System.Windows.Visibility.Visible;
                            logOutMenuItem.Visibility = System.Windows.Visibility.Visible;

                            logInMenuItem.Visibility = System.Windows.Visibility.Collapsed;
                            signUpMenuItem.Visibility = System.Windows.Visibility.Collapsed;
                        }
                        else
                        {
                            changePasswordMenuItem.Visibility = System.Windows.Visibility.Collapsed;
                            logOutMenuItem.Visibility = System.Windows.Visibility.Collapsed;

                            logInMenuItem.Visibility = System.Windows.Visibility.Visible;
                            signUpMenuItem.Visibility = System.Windows.Visibility.Visible;
                        }

                    }
                };
            // Set the data context of the listbox control to the sample data
            this.Loaded += new RoutedEventHandler(MainPage_Loaded);
        }

        // Load data for the ViewModel Items
        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("Main page laoded");

            //if (appbar == null)
            //{
            //    appbar = new AdvancedApplicationBar();
            //}

            //Binding loginBinding = new Binding("IsLoggedIn");
            //loginBinding.Source = this.DataContext;
            //loginBinding.Converter = new BooleanToVisibilityConverter();
            //loginBinding.ConverterParameter = Visibility.Collapsed;
            //loginBinding.Mode = BindingMode.TwoWay;

            //Binding loginBinding2 = new Binding("IsLoggedIn");
            //loginBinding2.Source = this.DataContext;
            //loginBinding2.Converter = new BooleanToVisibilityConverter();
            //loginBinding.ConverterParameter = Visibility.Visible;
            //loginBinding2.Mode = BindingMode.TwoWay;

            //var searchIconButton = new AdvancedApplicationBarIconButton() { 
            //    Text = Localization.search, 
            //    IconUri = new Uri("/Images/appbar.feature.search.rest.png", UriKind.Relative),
            //};
            //searchIconButton.Click += searchMenuItem_Click;
            //appbar.Items.Add(searchIconButton);

            //var scanIconButton = new AdvancedApplicationBarIconButton() { 
            //    Text = Localization.scanBarcode, 
            //    IconUri = new Uri("/Images/appbar.favs.camera.rest.png", UriKind.Relative) };
            //scanIconButton.Click += scanIconButton_Click;
            //appbar.Items.Add(scanIconButton);

            //var refreshButton = new AdvancedApplicationBarIconButton() { 
            //    Text = Localization.addToFavourites,
            //    IconUri = new Uri("/Images/appbar.refresh.rest.png", UriKind.Relative) };
            //refreshButton.Click += refreshButton_Click;
            //appbar.Items.Add(refreshButton);

            //var logInMenuItem = new AdvancedApplicationBarMenuItem() { Text = Localization.logIn };
            //logInMenuItem.Click += logInMenuItem_Click;
            //logInMenuItem.SetBinding(AdvancedApplicationBarMenuItem.VisibilityProperty, loginBinding);
            //appbar.Items.Add(logInMenuItem);

            //var signUpMenuItem = new AdvancedApplicationBarMenuItem() { Text = Localization.signUp };
            //logInMenuItem.SetBinding(AdvancedApplicationBarMenuItem.VisibilityProperty, loginBinding);
            //signUpMenuItem.Click += signUpMenuItem_Click;
            //appbar.Items.Add(signUpMenuItem);

            //var changePasswordMenuItem = new AdvancedApplicationBarMenuItem() { Text = Localization.changePassword };
            //logInMenuItem.SetBinding(AdvancedApplicationBarMenuItem.VisibilityProperty, loginBinding2);
            //changePasswordMenuItem.Click += changePasswordMenuItem_Click;
            //appbar.Items.Add(changePasswordMenuItem);

            //var logOutMenuItem = new AdvancedApplicationBarMenuItem() { Text = Localization.logOut };
            //logInMenuItem.SetBinding(AdvancedApplicationBarMenuItem.VisibilityProperty, loginBinding2);
            //logOutMenuItem.Click += logOutMenuItem_Click;
            //appbar.Items.Add(logOutMenuItem);

            //var settingsMenuItem = new AdvancedApplicationBarMenuItem() { Text = Localization.settings };
            //settingsMenuItem.Click += settingsMenuItem_Click;
            //appbar.Items.Add(settingsMenuItem);

            ////appbar.ButtonItems.Add(previewButton);
            ////appbar.ButtonItems.Add(addFavButton);
            ////appbar.ButtonItems.Add(refreshButton);
            //appbar.ReCreateAppBar();
        }

        

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            if (e.NavigationMode != System.Windows.Navigation.NavigationMode.Back)
            {
                if (State.ContainsKey(transientKey))
                {
                    State[transientKey] = ViewModel;
                }
                else
                {
                    State.Add(transientKey, ViewModel);
                }
            }

        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            Debug.WriteLine("mainpage on navigated to");

            if (isNewPageInstance)
            {
                if (ViewModel == null)
                {
                    object viewModel = null;

                    if (State.TryGetValue(transientKey, out viewModel))

                    {
                        DataContext = viewModel;
                        Debug.WriteLine("DataContextt");
                    }
                    else
                    {
                        DataContext = new MainViewModel();
                        ViewModel.LoadDataFromServer();

                        ViewModel.HistoryViewModel.UpdateRatings();
                        ViewModel.FavouriteBooksViewModel.UpdateRatings();
                    }
                }
            }

            if (BooksViewModel.LastSeenBookSysno != null)
            {
                ViewModel.UpdateRatings(BooksViewModel.LastSeenBookSysno);
            }

            isNewPageInstance = false;

            Debug.WriteLine("mainpage on navigated to End");
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {

        }

        private void Pivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Debug.WriteLine("Pivot selection changed");
            int pivotIndex = ((Pivot)sender).SelectedIndex;
            Debug.WriteLine("Pivot index" + pivotIndex);
            Binding visibilityBinding = null;
            switch(pivotIndex)
            {
                case 0:
                    visibilityBinding = new Binding("NewBooksViewModel.ConnectionErrorOccured");
                    appbar_refreshButton.Command = ViewModel.NewBooksViewModel.ReloadCommand;
                    break;
                case 1:
                    visibilityBinding = new Binding("TopBooksViewModel.ConnectionErrorOccured");
                    appbar_refreshButton.Command = ViewModel.TopBooksViewModel.ReloadCommand;
                    break;
                case 2:
                    visibilityBinding = new Binding("HistoryViewModel.ConnectionErrorOccured");
                    appbar_refreshButton.Command = ViewModel.HistoryViewModel.UpdateRatingsCommand;
                    break;
                case 3:
                    visibilityBinding = new Binding("FavouriteBooksViewModel.ConnectionErrorOccured");
                    appbar_refreshButton.Command = ViewModel.FavouriteBooksViewModel.UpdateRatingsCommand;
                    break;
            }

            Debug.WriteLine("Binding success");
            //if (errorPanel != null)
            //{
            //    visibilityBinding.Source = ViewModel;
            //    errorPanel.SetBinding(StackPanel.VisibilityProperty, visibilityBinding);
            //}

            if (selectionModeAppButton != null)
            {
                if (pivotIndex == 3)
                {
                    selectionModeAppButton.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    Debug.WriteLine("before visibility change");
                    selectionModeAppButton.Visibility = System.Windows.Visibility.Collapsed;
                    Debug.WriteLine("after visibility change");
                }
            }

            //if (errorPanel != null)
            //{
            //    if (((Pivot)sender).SelectedIndex == 0
            //        || ((Pivot)sender).SelectedIndex == 1)
            //    {
                        
            //    }
            //    else
            //    {
            //        errorPanel.Visibility = System.Windows.Visibility.Collapsed;
            //    }
            //}
        }

        private void AdvancedApplicationBar_Loaded(object sender, RoutedEventArgs e)
        {
            //(sender as AdvancedApplicationBar).BackgroundColor = Color.FromArgb(255,33,33,33);
            //(sender as AdvancedApplicationBar).ForegroundColor = Colors.White;
        }

        private StackPanel errorPanel = null;
        private void errorPanel_Loaded(object sender, RoutedEventArgs e)
        {
            if (sender is StackPanel)
                errorPanel = sender as StackPanel;
        }

        private void Pivot_Loaded(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("Pivot loaded");
        }
    }
}