﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Devices;
using System.Windows.Threading;
using System.Threading;
using System.Diagnostics;
using ZXing;
using ZXing.Common;

namespace WP7.ScanBarCode
{
    public partial class BarCode : PhoneApplicationPage
    {
        PhotoCamera _cam;
        VideoBrush _videoBrush = new VideoBrush();
        byte[] _buffer;
        Stopwatch watch = new Stopwatch();
        int _nbTry;
        Result result = null;

        public BarCode()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            isDisposed = false;
            result = null;
            _nbTry = 0;
            _cam = new PhotoCamera();

            _cam.Initialized += new EventHandler<CameraOperationCompletedEventArgs>(cam_Initialized);
            _cam.AutoFocusCompleted += cam_AutoFocusCompleted;

            video.Fill = _videoBrush;
            _videoBrush.SetSource(_cam);

            LoadingBar.Visibility = System.Windows.Visibility.Collapsed;

            base.OnNavigatedTo(e);
        }
        bool isDisposed = false;

        protected override void OnNavigatingFrom(System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            isDisposed = true;
            _cam.AutoFocusCompleted -= cam_AutoFocusCompleted;
            _cam.CancelFocus();
            _cam.Dispose();

            base.OnNavigatingFrom(e);
        }

        void cam_Initialized(object sender, CameraOperationCompletedEventArgs e)
        {
            _cam.FlashMode = FlashMode.Auto;
            _cam.Focus();
        }

        BinaryBitmap GetBitmapFromVideo(PhotoCamera cam)
        {
            if(isDisposed)
            {
                return null;
            }
            BinaryBitmap binaryBitmap = null;

            try
            {
                // Update buffer size    
                var pixelWidth = (int)_cam.PreviewResolution.Width;
                var pixelHeight = (int)_cam.PreviewResolution.Height;

                if (_buffer == null || _buffer.Length != (pixelWidth * pixelHeight))
                {
                    _buffer = new byte[pixelWidth * pixelHeight];
                }

                _cam.GetPreviewBufferY(_buffer);

                var luminance = new RGBLuminanceSource(_buffer, pixelWidth, pixelHeight, true);
                var binarizer = new HybridBinarizer(luminance);

                binaryBitmap = new BinaryBitmap(binarizer);
            }
            catch
            {
            }

            return binaryBitmap;
        }

        async void cam_AutoFocusCompleted(object sender, CameraOperationCompletedEventArgs e)
        {
            if (isDisposed)
                return;
            Debug.WriteLine("AutoFocused");
            if (result == null)
            { 
            try
            {
                _nbTry++;
                watch.Reset();
                watch.Start();

                while ((result == null) && (watch.ElapsedMilliseconds < 1500))
                {
                    var binaryBitmap = GetBitmapFromVideo(_cam);
                    if (binaryBitmap != null)
                    {
                        try
                        {
                            result = BarCodeManager.ZXingReader.decode(binaryBitmap);
                        }
                        catch(Exception ex)
                        {
                            // Wasn't able to find a barcode
                        }
                    }
                }

                if (result != null)
                {
                    Debug.WriteLine("Waiting for result processing.");
                     Deployment.Current.Dispatcher.BeginInvoke(() =>
                            {
                                LoadingBar.Visibility = System.Windows.Visibility.Visible;
                            });
                    if (!await BarCodeManager._onBarCodeFound(result))
                    {
                        Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {
                            LoadingBar.Visibility = System.Windows.Visibility.Collapsed;
                        });
                        result = null;
                        _nbTry = 0;
                        if (!isDisposed)
                        {
                            _cam.Focus();
                        }
                    }
                }
                else
                {
                    // Try to focus again
                    if (true)
                    {
                        if (!isDisposed)
                        {
                            _cam.Focus();
                        }
                    }
                    else
                    {
                        BarCodeManager._onError(new VideoScanException("Nothing was found during the scan"));
                    }
                }
            }
            catch (Exception exc)
            {
                BarCodeManager._onError(exc);
            }
        }
      }

    }
}