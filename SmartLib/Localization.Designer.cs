﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SmartLib
{
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Localization
    {

        private static global::System.Resources.ResourceManager resourceMan;

        private static global::System.Globalization.CultureInfo resourceCulture;

        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Localization()
        {
        }

        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager
        {
            get
            {
                if (object.ReferenceEquals(resourceMan, null))
                {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SmartLib.Localization", typeof(Localization).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }

        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture
        {
            get
            {
                return resourceCulture;
            }
            set
            {
                resourceCulture = value;
            }
        }


        public static string favourites
        {
            get
            {
                return ResourceManager.GetString("favourites", resourceCulture);
            }
        }

        public static string history
        {
            get
            {
                return ResourceManager.GetString("history", resourceCulture);
            }
        }

        public static string news
        {
            get
            {
                return ResourceManager.GetString("news", resourceCulture);
            }
        }

        public static string top
        {
            get
            {
                return ResourceManager.GetString("top", resourceCulture);
            }
        }

        public static string uco
        {
            get
            {
                return ResourceManager.GetString("uco", resourceCulture);
            }
        }

        public static string lastName
        {
            get
            {
                return ResourceManager.GetString("lastName", resourceCulture);
            }
        }

        public static string firstName
        {
            get
            {
                return ResourceManager.GetString("firstName", resourceCulture);
            }
        }

        public static string signUp
        {
            get
            {
                return ResourceManager.GetString("signUp", resourceCulture);
            }
        }

        public static string logIn
        {
            get
            {
                return ResourceManager.GetString("logIn", resourceCulture);
            }
        }

        public static string skip
        {
            get
            {
                return ResourceManager.GetString("skip", resourceCulture);
            }
        }

        public static string password
        {
            get
            {
                return ResourceManager.GetString("password", resourceCulture);
            }
        }

        public static string savePassword
        {
            get
            {
                return ResourceManager.GetString("savePassword", resourceCulture);
            }
        }

        public static string oldPassword
        {
            get
            {
                return ResourceManager.GetString("oldPassword", resourceCulture);
            }
        }

        public static string newPassword
        {
            get
            {
                return ResourceManager.GetString("newPassword", resourceCulture);
            }
        }

        public static string newPasswordAgain
        {
            get
            {
                return ResourceManager.GetString("newPasswordAgain", resourceCulture);
            }
        }

        public static string changePassword
        {
            get
            {
                return ResourceManager.GetString("changePassword", resourceCulture);
            }
        }

        public static string review
        {
            get
            {
                return ResourceManager.GetString("review", resourceCulture);
            }
        }

        public static string save
        {
            get
            {
                return ResourceManager.GetString("save", resourceCulture);
            }
        }

        public static string rating
        {
            get
            {
                return ResourceManager.GetString("rating", resourceCulture);
            }
        }

        public static string reviewText
        {
            get
            {
                return ResourceManager.GetString("reviewText", resourceCulture);
            }
        }

        public static string reviews
        {
            get
            {
                return ResourceManager.GetString("reviews", resourceCulture);
            }
        }

        public static string details
        {
            get
            {
                return ResourceManager.GetString("details", resourceCulture);
            }
        }

        public static string text
        {
            get
            {
                return ResourceManager.GetString("text", resourceCulture);
            }
        }

        public static string search
        {
            get
            {
                return ResourceManager.GetString("search", resourceCulture);
            }
        }

        public static string scanBarcode
        {
            get
            {
                return ResourceManager.GetString("scanBarcode", resourceCulture);
            }
        }

        public static string results
        {
            get
            {
                return ResourceManager.GetString("results", resourceCulture);
            }
        }

        public static string settings
        {
            get
            {
                return ResourceManager.GetString("settings", resourceCulture);
            }
        }

        public static string resultsCount
        {
            get
            {
                return ResourceManager.GetString("resultsCount", resourceCulture);
            }
        }

        public static string publisher
        {
            get
            {
                return ResourceManager.GetString("publisher", resourceCulture);
            }
        }

        public static string published
        {
            get
            {
                return ResourceManager.GetString("published", resourceCulture);
            }
        }

        public static string language
        {
            get
            {
                return ResourceManager.GetString("language", resourceCulture);
            }
        }

        public static string pages
        {
            get
            {
                return ResourceManager.GetString("pages", resourceCulture);
            }
        }

        public static string availability
        {
            get
            {
                return ResourceManager.GetString("availability", resourceCulture);
            }
        }

        public static string checkNow
        {
            get
            {
                return ResourceManager.GetString("checkNow", resourceCulture);
            }
        }

        public static string addReview
        {
            get
            {
                return ResourceManager.GetString("addReview", resourceCulture);
            }
        }

        public static string preview
        {
            get
            {
                return ResourceManager.GetString("preview", resourceCulture);
            }
        }

        public static string addToFavourites
        {
            get
            {
                return ResourceManager.GetString("addToFavourites", resourceCulture);
            }
        }

        public static string removeFromFavourites
        {
            get
            {
                return ResourceManager.GetString("removeFromFavourites", resourceCulture);
            }
        }

        public static string logOut
        {
            get
            {
                return ResourceManager.GetString("logOut", resourceCulture);
            }
        }

        public static string reload
        {
            get
            {
                return ResourceManager.GetString("reload", resourceCulture);
            }
        }

        public static string login_error1
        {
            get
            {
                return ResourceManager.GetString("login_error1", resourceCulture);
            }
        }


        public static string login_success
        {
            get
            {
                return ResourceManager.GetString("login_success", resourceCulture);
            }
        }

        public static string login_error2
        {
            get
            {
                return ResourceManager.GetString("login_error2", resourceCulture);
            }
        }

        public static string server_error
        {
            get
            {
                return ResourceManager.GetString("server_error", resourceCulture);
            }
        }

        public static string preview_error
        {
            get
            {
                return ResourceManager.GetString("preview_error", resourceCulture);
            }
        }

        public static string fav_added
        {
            get
            {
                return ResourceManager.GetString("fav_added", resourceCulture);
            }
        }

        public static string fav_removed
        {
            get
            {
                return ResourceManager.GetString("fav_removed", resourceCulture);
            }
        }

        public static string search_noresult
        {
            get
            {
                return ResourceManager.GetString("search_noresult", resourceCulture);
            }
        }

        public static string logout_success
        {
            get
            {
                return ResourceManager.GetString("logout_success", resourceCulture);
            }
        }

        public static string logout_error
        {
            get
            {
                return ResourceManager.GetString("logout_error", resourceCulture);
            }
        }

        public static string review_saved
        {
            get
            {
                return ResourceManager.GetString("review_saved", resourceCulture);
            }
        }

        public static string need_auth
        {
            get
            {
                return ResourceManager.GetString("need_auth", resourceCulture);
            }
        }

        public static string password_changed
        {
            get
            {
                return ResourceManager.GetString("password_changed", resourceCulture);
            }
        }

        public static string signup_succ
        {
            get
            {
                return ResourceManager.GetString("signup_succ", resourceCulture);
            }
        }

        public static string signup_exists
        {
            get
            {
                return ResourceManager.GetString("signup_exists", resourceCulture);
            }
        }

        public static string signup_notexists
        {
            get
            {
                return ResourceManager.GetString("signup_notexists", resourceCulture);
            }
        }

        public static string enterthe
        {
            get
            {
                return ResourceManager.GetString("enterthe", resourceCulture);
            }
        }

        public static string repeatedPasswordDiff
        {
            get
            {
                return ResourceManager.GetString("repeatedPasswordDiff", resourceCulture);
            }
        }

        public static string rateBook
        {
            get
            {
                return ResourceManager.GetString("rateBook", resourceCulture);
            }
        }

        public static string invalidUco
        {
            get
            {
                return ResourceManager.GetString("invalidUco", resourceCulture);
            }
        }

        public static string checkConnection
        {
            get
            {
                return ResourceManager.GetString("checkConnection", resourceCulture);
            }
        }

        public static string available
        {
            get
            {
                return ResourceManager.GetString("available", resourceCulture);
            }
        }

        public static string unavailable
        {
            get
            {
                return ResourceManager.GetString("unavailable", resourceCulture);
            }
        }

        public static string library
        {
            get
            {
                return ResourceManager.GetString("library", resourceCulture);
            }
        }

        public static string status
        {
            get
            {
                return ResourceManager.GetString("status", resourceCulture);
            }
        }

        public static string signature
        {
            get
            {
                return ResourceManager.GetString("signature", resourceCulture);
            }
        }

        public static string remove
        {
            get
            {
                return ResourceManager.GetString("remove", resourceCulture);
            }
        }

        public static string edit
        {
            get
            {
                return ResourceManager.GetString("edit", resourceCulture);
            }
        }

        public static string back
        {
            get
            {
                return ResourceManager.GetString("back", resourceCulture);
            }
        }

        public static string lostPassword
        {
            get
            {
                return ResourceManager.GetString("lostPassword", resourceCulture);
            }
        }

        public static string lostPassword_succ
        {
            get
            {
                return ResourceManager.GetString("lostPassword_succ", resourceCulture);
            }
        }

        public static string author
        {
            get
            {
                return ResourceManager.GetString("author", resourceCulture);
            }
        }

        public static string title
        {
            get
            {
                return ResourceManager.GetString("title", resourceCulture);
            }
        }

        public static string isbn
        {
            get
            {
                return ResourceManager.GetString("isbn", resourceCulture);
            }
        }

        public static string search_validation_msg
        {
            get
            {
                return ResourceManager.GetString("search_validation_msg", resourceCulture);
            }
        }

        public static string onSite
        {
            get
            {
                return ResourceManager.GetString("onSite", resourceCulture);
            }
        }

        public static string offSite
        {
            get
            {
                return ResourceManager.GetString("offSite", resourceCulture);
            }
        }

        public static string share
        {
            get
            {
                return ResourceManager.GetString("share", resourceCulture);
            }
        }
    }
}
