﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using NLog.Targets;
using NLog;
using System.IO.IsolatedStorage;
using System.IO;

namespace SmartLib
{
    [Target("IsolatedStorage")]
    public sealed class IsolatedStorageTarget : TargetWithLayout
    {
        public IsolatedStorageTarget()
        {
        }
        protected override void Write(LogEventInfo logEvent)
        {
            try
            {
                using (IsolatedStorageFile store =
                       IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (Stream stream = new IsolatedStorageFileStream
                           ("log.txt", FileMode.Append, FileAccess.Write, store))
                    {
                        StreamWriter writer = new StreamWriter(stream);
                        writer.WriteLine(this.Layout.Render(logEvent));
                        writer.Close();
                    }                    
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
