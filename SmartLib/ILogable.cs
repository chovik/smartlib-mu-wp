﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartLib.Events;

namespace SmartLib.Core
{
    public interface ILogable
    {
        event EventHandler<SuccessEventArgs> SuccessEvent;

        event EventHandler<ErrorEventArgs> ErrorEvent;
    }
}
