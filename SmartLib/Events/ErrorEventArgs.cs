﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using SmartLib.Helpers;

namespace SmartLib.Events
{
    public class ErrorEventArgs : EventArgs
    {
        private string message = null;

        public string Message
        {
            get
            {
                return Message;
            }
            set
            {
                ArgumentValidator.AssertNotNullOrWhiteSpaceString(value, "Message");

                if (value != message)
                {
                    message = value;
                }
            }
        }

        public ErrorEventArgs(string message)
        {
            Message = message;
        }
    }
}
