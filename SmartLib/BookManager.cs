﻿using SmartLib.Models;
using SmartLib.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;

namespace SmartLib
{
    public class BookManager
    {
        private static BookManager instance;
        public static BookManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BookManager();
                }

                return instance;
            }
        }

        public BookManager()
        {
        }

        //key is sysno
        public Dictionary<string, BookViewModel> CachedBooks = new Dictionary<string,BookViewModel>();

        private ImpObservableCollection<BookViewModel> topRatedBooks;

        public ImpObservableCollection<BookViewModel> TopRatedBooks
        {
            get 
            {
                if (topRatedBooks == null)
                {
                    topRatedBooks = new ImpObservableCollection<BookViewModel>();
                }
                return topRatedBooks; 
            }
        }

        private ImpObservableCollection<BookViewModel> recentlyRatedBooks;

        public ImpObservableCollection<BookViewModel> RecentlyRatedBooks
        {
            get
            {
                if (recentlyRatedBooks == null)
                {
                    recentlyRatedBooks = new ImpObservableCollection<BookViewModel>();
                }
                return recentlyRatedBooks;
            }
        }

        private ImpObservableCollection<BookViewModel> favouriteBooks;

        public ImpObservableCollection<BookViewModel> FavouriteBooks
        {
            get
            {
                if (favouriteBooks == null)
                {
                    favouriteBooks = new ImpObservableCollection<BookViewModel>();
                    favouriteBooks.ItemsAdded += (s, e) =>
                        {
                            foreach (BookViewModel book in e.Items)
                            {
                                if (book != null)
                                {
                                    book.IsBookmarked = true;
                                }
                            }
                        };
                    favouriteBooks.ItemsRemoved += (s, e) =>
                        {
                            foreach (BookViewModel book in e.Items)
                            {
                                if (book != null)
                                {
                                    book.IsBookmarked = false;
                                }
                            }
                        };
                }
                return favouriteBooks;
            }
        }

        private ObservableCollection<BookViewModel> history;

        public ObservableCollection<BookViewModel> History
        {
            get
            {
                if (history == null)
                {
                    history = new ObservableCollection<BookViewModel>();
                }
                return history;
            }
        }

        public void RemoveBookFromHistory(BookViewModel book)
        {
            History.Remove(book);
        }

        public void AddBookToHistory(BookViewModel book)
        {
            var foundBooks = History.Where(b => b != null && b.Book != null 
                && book != null && book.Book != null 
                && b.Book.Sysno == book.Book.Sysno);
            if (foundBooks.Count() > 0)
            {
                var toRemove = foundBooks.ToArray();
                foreach (var b in toRemove)
                {
                    History.Remove(b);
                }
            }
            History.Insert(0, book);
            if (History.Count > 10)
            {
                History.RemoveAt(10);
            }
        }

        public async Task<BookViewModel> GetBook(string sysno)
        {
            if (CachedBooks.ContainsKey(sysno))
            {
                return CachedBooks[sysno];
            }
            else
            {
                try
                {
                    var book = await App.CurrentApplication.BookRequests.GetBookBy(BookIdentifier.SysNo, sysno);

                    if (book != null)
                    {
                        if (CachedBooks.ContainsKey(sysno))
                        {
                            return CachedBooks[sysno];
                        }
                        else
                        {
                            var newBookViewModel = new BookViewModel(book);
                            CachedBooks.Add(sysno, newBookViewModel);
                            return newBookViewModel;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public BookViewModel AddBook(Book book)
        {
            if (CachedBooks.ContainsKey(book.Sysno))
            {
                var bookViewModel = CachedBooks[book.Sysno];
                //bookViewModel.Book = book;
                bookViewModel.Book.RatingCount = book.RatingCount;
                bookViewModel.Book.averageRating = book.averageRating;
                return bookViewModel;
            }
            else
            {
                try
                {
                    var newBookViewModel = new BookViewModel(book);
                    CachedBooks.Add(book.Sysno, newBookViewModel);
                    return newBookViewModel;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public async Task LoadTopRatedBooks()
        {
            TopRatedBooks.Clear();

            try
            {
                var topBooks = await App.CurrentApplication.BookRequests.GetBooksByCategory(BookListCategory.Top, 10, 0);

                if (topBooks != null)
                {
                    foreach (var topBook in topBooks)
                    {
                        var topBookViewModel = AddBook(topBook);
                        if (topBookViewModel != null)
                        {
                            TopRatedBooks.Add(topBookViewModel);
                        }

                    }
                }
            }
            catch(Exception ex)
            {
            }
        }

        public async Task LoadRecentlyRatedBooks()
        {
            RecentlyRatedBooks.Clear();
            try
            {
                var books = await App.CurrentApplication.BookRequests.GetBooksByCategory(BookListCategory.News, 10, 0);

                if (books != null)
                {
                    foreach (var book in books)
                    {
                        var bookViewModel = AddBook(book);

                        if (bookViewModel != null)
                        {
                            RecentlyRatedBooks.Add(bookViewModel);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
            }
        }

        public async Task<BookViewModel> GetDetailedBook(string sysno)
        {
            var book = await GetBook(sysno);

            if (book != null)
            {
                book.Book = await App.CurrentApplication.BookRequests.GetBookDetails(book.Book);

                return book;
            }
            else
            {
                return null;
            }
        }

        public async void LoadHistory(IEnumerable<string> sysnos)
        {
            History.Clear();
            foreach(string sysno in sysnos)
            {
                var book = await GetBook(sysno);
                if (book != null)
                {
                    History.Add(book);
                }
            }
        }

        public async void LoadFavourites(IEnumerable<string> sysnos)
        {
            FavouriteBooks.Clear();
            foreach (string sysno in sysnos)
            {
                var book = await GetBook(sysno);
                FavouriteBooks.Add(book);
            }
        }


        public void LoadCachedBooks(Dictionary<string, BookViewModel> books)
        {
            CachedBooks = books ?? new Dictionary<string, BookViewModel>();
        }
    }
}
