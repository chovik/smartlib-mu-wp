﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartLib.Helpers
{
    public interface ICloneable
    {
        object Clone();
    }
}
