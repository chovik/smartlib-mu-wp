﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Diagnostics;
using System.Text;

namespace SmartLib.Helpers
{
    public static class JSONParser
    {
        /// <summary>
        /// Parses JSON data to object of specified type.
        /// </summary>
        /// <typeparam name="T">type of returned object</typeparam>
        /// <param name="json">JSON data</param>
        /// <returns>object of specified type parsed from JSON data</returns>
        public static async Task<T> ParseDataAsync<T>(string json)
        {
            //if (json == null)
            //    throw new ArgumentNullException("json");

            Debug.WriteLine("Parsing data. JSON:\n {0}\n", json);

            T data = default(T);
            try
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
                //{
                data = await TaskEx.Run(() => (T)serializer.ReadObject(ms));
                string myString = string.Format("Parsing data - Successful. JSON:\n {0}\n", json);

                for (int i = 0; i < myString.Length; i += 714)
                {
                    Debug.WriteLine(myString.Substring(i, Math.Min(714, myString.Length - i)));
                }
                //}
            }
            catch (Exception ex)
            {
                string myString = string.Format("Parsing data - Error. JSON:\n {0}\n Exceptin:\n {1}\n", json, ex.Message);

                for (int i = 0; i < myString.Length; i += 714)
                {
                    Debug.WriteLine(myString.Substring(i, Math.Min(714, myString.Length - i)));
                }

                myString = string.Format("Parsing exception: {0}", ex);

                for (int i = 0; i < myString.Length; i += 714)
                {
                    Debug.WriteLine(myString.Substring(i, Math.Min(714, myString.Length - i)));
                }
            }

            return data;
        }
    }
}
